package star.neo.fragments;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gc.materialdesign.views.ButtonFloat;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import star.neo.adapters.CommentViewAdapter;
import star.neo.models.CommentsModel;
import star.neo.starradionew.R;
import star.neo.utils.Config;
import star.neo.utils.JsonParser;
import star.neo.utils.UrlConstant;


/**
 * A simple {@link Fragment} subclass.
 */
public class LiveCommentFragment extends Fragment {


    ProgressDialog progressDialog;

    CircleImageView sendCommentButton;

    EditText comment;

    List<CommentsModel> commentsModelList;

    RecyclerView recyclerView;

    CommentViewAdapter adapter;

    CoordinatorLayout coordinatorLayout;

    ProgressBar progressBar;

    String commentRequest, accessToken;

    LinearLayout bottomLinearLayout;

    public LiveCommentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_live_comment, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.comment_send));

        SharedPreferences preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);

        accessToken = preferences.getString(Config.ACCESS_TOKEN, "");

        sendCommentButton = (CircleImageView) view.findViewById(R.id.fab);
        comment = (EditText) view.findViewById(R.id.comment_field);

        bottomLinearLayout = (LinearLayout) view.findViewById(R.id.down_things);

        sendCommentButton.setEnabled(false);

        coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinatorLayoutComment);

        sendCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendComment();
            }
        });

        comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.toString().length() > 0) {
                    sendCommentButton.setEnabled(true);
                }
            }
        });

        loadComment();


        return view;
    }

    public void sendComment() {

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        commentRequest = comment.getText().toString();

        progressDialog.show();

        StringRequest postRequest = new StringRequest(Request.Method.POST, UrlConstant.ADD_COMMENTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response != null) {

                            progressDialog.dismiss();

                            if (JsonParser.checkStatus(response)) {

                                comment.setText("");

                                try {


                                    SharedPreferences preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString(Config.COMMENT_JSON_RESPONSE, response);
                                    editor.apply();

                                    processJsonResponse(response);

                                } catch (Exception ex) {

                                }

                            }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();

                Toast.makeText(getActivity(), "Oops:) something went wrong. please check your internet connection", Toast.LENGTH_LONG).show();

            }
        }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put(Config.ACCESS_TOKEN, accessToken);
                params.put(Config.COMMENT_PARAM, commentRequest);

                return params;
            }
        };

        queue.add(postRequest);
    }

    public void loadComment() {

        showProgress(true);

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest strReq = new StringRequest(Request.Method.GET,
                UrlConstant.GET_ALL_COMMENTS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                if (response != null) {

                    showProgress(false);

//                   cache fresh comments
                    try {
                        SharedPreferences preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(Config.COMMENT_JSON_RESPONSE, response);
                        editor.apply();

                        processJsonResponse(response);

                    } catch (Exception ex) {

                    }

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


               try{
                   showProgress(false);

                   Toast.makeText(getActivity(), "Oops:) something went wrong. please check your internet connection", Toast.LENGTH_LONG).show();

                   SharedPreferences preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);
                   String jsonResponse = preferences.getString(Config.COMMENT_JSON_RESPONSE, "");

                   processJsonResponse(jsonResponse);
               }
               catch(Exception ex){

               }
            }
        });

        //Adding request to request queue
        queue.add(strReq);
    }

    public void processJsonResponse(String response) {

        commentsModelList = JsonParser.parseLiveComment(response);

        if(!(commentsModelList.size() > 0))
            Toast.makeText(getActivity(), "No comment found", Toast.LENGTH_LONG).show();


        Log.d("commentSize", String.valueOf(commentsModelList.size()));

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new CommentViewAdapter(commentsModelList, getActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.scrollToPosition(commentsModelList.size() - 1);
    }

    private void showProgress(boolean isShowing) {
        if (isShowing) {

            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

}
