package star.neo.adapters;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import star.neo.models.EpisodeModel;
import star.neo.starradionew.R;
import star.neo.utils.Config;
import star.neo.utils.InputStreamVolleyRequest;
import star.neo.utils.MySingleton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * Created by mesh on 6/26/16.
 */
public class EpisodeAdapter extends RecyclerView.Adapter<EpisodesViewHolder> {

    List<EpisodeModel> episodeModelList = Collections.emptyList();
    Context context;

    ImageLoader mImageLoader;

    public EpisodeAdapter(List<EpisodeModel> episodeModelList, Context context) {

        this.episodeModelList = episodeModelList;
        this.context = context;
    }


    @Override
    public EpisodesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_episode_row_layout, parent, false);

        EpisodesViewHolder viewHolder = new EpisodesViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(EpisodesViewHolder holder, final int position) {

        mImageLoader = MySingleton.getInstance(context.getApplicationContext())
                .getImageLoader();

        mImageLoader.get(episodeModelList.get(position).getEpisodeUrl(),
                ImageLoader.getImageListener(holder.episodeImage, R.drawable.default_art, R.drawable.default_art));

        holder.episodeNameView.setText(episodeModelList.get(position).getTitle());
        holder.episodeDescView.setText(episodeModelList.get(position).getDescription());
        holder.episodeDownloadNumberView.setText(String.valueOf(" " + episodeModelList.get(position).getNumberOfDownloads()));
        holder.episodeTimeCreated.setText(episodeModelList.get(position).getDatePublished());
        holder.episodeStreamView.setText(String.valueOf(" " + episodeModelList.get(position).getNumberOfPlayingStream()));

        holder.downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(context)
                        .title(episodeModelList.get(position).getTitle())
                        .content(episodeModelList.get(position).getDescription())
                        .positiveText("Download")
                        .negativeText("Dismiss")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                            @Override
                            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {



                                final int id = 1;

                                final Notification.Builder builder = new Notification.Builder(context);

                                final NotificationManager notificationManager = (NotificationManager)
                                        context.getSystemService(Context.NOTIFICATION_SERVICE);

                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.fromFile(getOutputMediaFile("")), "audio/*");

                                PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);


                                builder.setContentTitle("Star Radio Episode Download")
                                        .setContentText("Download in progress")
                                        .setSmallIcon(R.mipmap.ic_launcher)
                                        .setContentIntent(pendingIntent);


                                builder.setProgress(0, 0, true);
                                notificationManager.notify(id, builder.build());

                                String mUrl = episodeModelList.get(position).getDownloadUrl();

                                InputStreamVolleyRequest request = new InputStreamVolleyRequest(Request.Method.GET, mUrl,
                                        new Response.Listener<byte[]>() {
                                            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                                            @Override
                                            public void onResponse(byte[] response) {
                                                // TODO handle the response
                                                try {
                                                    if (response != null) {

                                                        Log.d("star radio", "hello i am here");

                                                        String fileName = episodeModelList.get(position).getTitle() + ".mp3";

                                                        FileOutputStream outputStream;

                                                        outputStream = new FileOutputStream(getOutputMediaFile(fileName));

                                                        outputStream.write(response);

                                                        outputStream.close();

                                                        builder.setContentText("Download complete")
                                                                .setProgress(0, 0, false);
                                                        notificationManager.notify(id, builder.build());
                                                    }
                                                } catch (Exception e) {
                                                    // TODO Auto-generated catch block
                                                    Log.d("KEY_ERROR", "UNABLE TO DOWNLOAD FILE");
                                                    e.printStackTrace();
                                                }
                                            }
                                        }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        // TODO handle the error
                                        error.printStackTrace();
                                    }
                                }, null);
                                RequestQueue mRequestQueue = Volley.newRequestQueue(context, new HurlStack());
                                mRequestQueue.add(request);
                            }
                        })
                        .positiveColor(ContextCompat.getColor(context, R.color.md_green_400))
                        .negativeColor(ContextCompat.getColor(context, R.color.md_red_400))
                        .show();
            }
        });

    }


    @Override
    public int getItemCount() {
//        return 0;
        try {
            return episodeModelList.size();
        } catch (Exception ex) {

            ex.printStackTrace();
            return 0;
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    private static File getOutputMediaFile(String fileName) {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                Config.MP3_FOLDER);

        if (!mediaStorageDir.exists()) {

            if (!mediaStorageDir.mkdirs()) {
                Log.d("episode", "failed to create videos in the directory " + Config.MP3_FOLDER);
                return null;
            }
        }

        Date date = new Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date.getTime());

        File mediaFile;

        mediaFile = new File(mediaStorageDir.getPath() + File.separator + fileName + timeStamp + ".mp3");

        if (!mediaFile.exists()) {
            try {
                mediaFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return mediaFile;
    }




}
