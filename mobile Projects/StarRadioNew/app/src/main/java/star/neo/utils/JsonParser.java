package star.neo.utils;

import android.text.Html;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import star.neo.models.CommentsModel;
import star.neo.models.EpisodeModel;
import star.neo.models.ImageModel;
import star.neo.models.NewsModel;
import star.neo.models.PresentersTeamModel;
import star.neo.models.SchedulesModel;
import star.neo.models.UserModel;


/**
 * Created by mesh on 6/22/16.
 */
public class JsonParser {

    public static boolean checkStatus (String response){

        boolean status = false;

        try {
            JSONObject jsonObject = new JSONObject(response);

            status = jsonObject.getBoolean("status");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return status;
    }


    public static List<NewsModel> parseGetNews(String response) {

        List<NewsModel> newsModelList = new ArrayList<>();

        try {

            JSONObject jsonObject = new JSONObject(response);

            JSONObject responseDataObject = jsonObject.getJSONObject("responseData");

            JSONObject feedJsonObject = responseDataObject.getJSONObject("feed");

            JSONArray jsonArray = feedJsonObject.getJSONArray("entries");

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObjectArrayElement = jsonArray.getJSONObject(i);

                NewsModel newsModel = new NewsModel();

                newsModel.setNewsTitle(jsonObjectArrayElement.getString("title"));
                newsModel.setNewSubContent(jsonObjectArrayElement.getString("contentSnippet"));
                newsModel.setNewsTimeCreated(jsonObjectArrayElement.getString("publishedDate"));
                newsModel.setNewsAuthor(jsonObjectArrayElement.getString("author"));
                newsModel.setFullNewsContent(jsonObjectArrayElement.getString("content"));
                newsModel.setNewsLink(jsonObjectArrayElement.getString("link"));

                String html = jsonObjectArrayElement.getString("content");

                if (Utility.extractImgFromHtml(html) != null){
                    newsModel.setNewsImageUrl(Utility.extractImgFromHtml(html));

                }


                JSONArray categoryArray = jsonObjectArrayElement.getJSONArray("categories");

                for (int j = 0; j < 1; j++) {

                    String categoryObjectArrayElement = categoryArray.getString(j);

//                    Log.d("categoryArray", String.valueOf(categoryObjectArrayElement));

                    newsModel.setCategoryName(categoryObjectArrayElement);
                }

                newsModelList.add(newsModel);

            }

            return newsModelList;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<CommentsModel> parseLiveComment (String dataJson){

        List<CommentsModel> commentsModelList = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(dataJson);

            JSONArray commentArray = jsonObject.getJSONArray("data");

            for (int i = 0; i < commentArray.length(); i++) {

                JSONObject commentObjectElement = commentArray.getJSONObject(i);

                CommentsModel commentsModel = new CommentsModel();

                String firstName = commentObjectElement.getString("firstname");
                String lastName = commentObjectElement.getString("lastname");

                if(!TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(lastName))
                    commentsModel.setCommentCreatorName(firstName + " " + lastName);

                else
                    commentsModel.setCommentCreatorName(commentObjectElement.getString("username"));

                commentsModel.setComment(commentObjectElement.getString("comments"));
                commentsModel.setCommentTime(commentObjectElement.getString("time_created"));
                commentsModel.setCommentCreatorImageUrl(commentObjectElement.getString("image_url"));

                commentsModelList.add(commentsModel);

            }

            return commentsModelList;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<EpisodeModel> parseEpisodes (String response){

        List<EpisodeModel> episodeModelList = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(response);

            JSONObject responseObject = jsonObject.getJSONObject("response");

            JSONObject pagerObject = responseObject.getJSONObject("pager");

            JSONArray resultArray = pagerObject.getJSONArray("results");


            for (int i = 0; i < resultArray.length(); i++) {

                JSONObject resultArrayElement = resultArray.getJSONObject(i);

                JSONObject authorObjectElement = resultArrayElement.getJSONObject("author");

                JSONObject statsObject = resultArrayElement.getJSONObject("stats");

                JSONObject showObject = resultArrayElement.getJSONObject("show");

                EpisodeModel episodeModel = new EpisodeModel();
                episodeModel.setTitle(resultArrayElement.getString("title"));
                episodeModel.setDatePublished(resultArrayElement.getString("published_at_locale"));
                episodeModel.setDownloadUrl(resultArrayElement.getString("download_url"));
                episodeModel.setEpisodeId(resultArrayElement.getInt("episode_id"));
                episodeModel.setNumberOfDownloads(statsObject.getInt("plays_download"));
                episodeModel.setNumberOfPlayingStream(statsObject.getInt("plays_streaming"));
                episodeModel.setDescription(authorObjectElement.getString("description"));

                JSONObject imageObject = showObject.getJSONObject("image");
                episodeModel.setEpisodeUrl(imageObject.getString("large_url"));

                episodeModelList.add(episodeModel);
            }

            return episodeModelList;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    public static String parseFeedbackMessage(String response){

        try {
            JSONObject jsonObject = new JSONObject(response);

            return jsonObject.getString("data");

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getTokenFromRegistration(String response){

        try {
            JSONObject jsonObject = new JSONObject(response);

            return jsonObject.getString("token");

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getTokenFromLogin(String response){

        try {
            String result = null;

            JSONObject jsonObject = new JSONObject(response);

            JSONArray responseArray = jsonObject.getJSONArray("data");

            for (int i = 0; i < responseArray.length(); i++) {

                JSONObject objectElement = responseArray.getJSONObject(i);

                result = objectElement.getString("google_access_token");

            }

            return result;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<SchedulesModel> parseSchedule(String response){

        List<SchedulesModel> schedulesModelList = new ArrayList<>();

        try {
            JSONObject  jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("data");

            for (int i = 0; i < jsonArray.length(); i++) {

                SchedulesModel schedulesModel = new SchedulesModel();

                JSONObject jsonArrayObjectElement = jsonArray.getJSONObject(i);
                schedulesModel.setSchedule(jsonArrayObjectElement.getString("schedule"));
                schedulesModel.setScheduleTimeFrom(jsonArrayObjectElement.getString("schedule_time_from"));
                schedulesModel.setScheduleTimeTo(jsonArrayObjectElement.getString("schedule_time_to"));
                schedulesModel.setScheduleImageClipArtUrl(jsonArrayObjectElement.getString("schedule_clip_art_image"));

                boolean status = jsonArrayObjectElement.getString("schedule_status").equals("1");
                schedulesModel.setScheduleStatus(status);
                schedulesModel.setScheduleFeaturedName(jsonArrayObjectElement.getString("schedule_feature_name"));

                schedulesModelList.add(schedulesModel);
            }

            return schedulesModelList;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<PresentersTeamModel> parseTeamPresenters(String response){

        List<PresentersTeamModel> presentersTeamModelList = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(response);

            JSONArray jsonArray = jsonObject.getJSONArray("data");

            for (int i = 0; i < jsonArray.length(); i++) {

                PresentersTeamModel presentersTeamModel = new PresentersTeamModel();

                JSONObject jsonArrayObjectElement = jsonArray.getJSONObject(i);
                presentersTeamModel.setUsername(jsonArrayObjectElement.getString("username"));
                presentersTeamModel.setRoleModel(jsonArrayObjectElement.getString("presenter_role_model"));
                presentersTeamModel.setBiography(jsonArrayObjectElement.getString("presenter_bio"));
                presentersTeamModel.setImageUrl(jsonArrayObjectElement.getString("image_url"));

                presentersTeamModelList.add(presentersTeamModel);
            }

            return presentersTeamModelList;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static List<UserModel> parseManualLogin(String response){

        List<UserModel> userModelList = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(response);

            JSONArray jsonArray = jsonObject.getJSONArray("data");

            for (int i = 0; i < jsonArray.length(); i++) {

                UserModel userModel = new UserModel();

                JSONObject jsonArrayObjectElement = jsonArray.getJSONObject(i);
                userModel.setUsername(jsonArrayObjectElement.getString("username"));
                userModel.setEmail(jsonArrayObjectElement.getString("email"));
                userModel.setAccessToken(jsonArrayObjectElement.getString("google_access_token"));
                userModel.setFirstname(jsonArrayObjectElement.getString("firstname"));
                userModel.setLastname(jsonArrayObjectElement.getString("lastname"));
                userModel.setCountry(jsonArrayObjectElement.getString("country"));
                userModel.setImageUrl(jsonArrayObjectElement.getString("image_url"));


                userModelList.add(userModel);
            }

            return userModelList;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String parseImageUrl(String response){
        try {
            JSONObject jsonObject = new JSONObject(response);

            return jsonObject.getString("imageurl");

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ImageModel parseUserImageProfile(String response){

        try {

            JSONObject jsonObject = new JSONObject(response);

            ImageModel imageModel = new ImageModel();

            imageModel.setFeedback(jsonObject.getString("data"));

            imageModel.setImageUrl(jsonObject.getString("imageurl"));

            return imageModel;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


}
