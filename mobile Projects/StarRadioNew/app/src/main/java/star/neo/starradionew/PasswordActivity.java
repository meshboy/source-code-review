package star.neo.starradionew;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import star.neo.utils.Config;
import star.neo.utils.JsonParser;
import star.neo.utils.UrlConstant;


public class PasswordActivity extends AppCompatActivity {

    private ProgressDialog progressDialog, paymentStatusDialog;

    String username, email, country, imageUrl, firstname, lastname, password;

    EditText passwordField;

    TextView emailTextView;

    View focusView = null;
    boolean cancel = false , paymentStatus = false, isLogin;

    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);

        actionBar = getSupportActionBar();

        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        SpannableString spannableString =  new SpannableString(getResources().getString(R.string.loading));
        spannableString.setSpan(new ForegroundColorSpan(Color.BLACK), 0, spannableString.length(), 0);

        SpannableString spannableString1 =  new SpannableString(getResources().getString(R.string.verify_payment));
        spannableString1.setSpan(new ForegroundColorSpan(Color.BLACK), 0, spannableString1.length(), 0);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(spannableString);

        paymentStatusDialog = new ProgressDialog(this);
        paymentStatusDialog.setMessage(spannableString1);

        Intent intent = getIntent();
        username = intent.getStringExtra("username");
        email = intent.getStringExtra("email");
        country = intent.getStringExtra("country");
        imageUrl = intent.getStringExtra("imageUrl");
        firstname = intent.getStringExtra("firstname");
        lastname = intent.getStringExtra("lastname");

        passwordField = (EditText) findViewById(R.id.password);

        emailTextView = (TextView) findViewById(R.id.email_address);
        emailTextView.setText(email);

//        findViewById(R.id.authenticateButton).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                onButtonClick();
//            }
//        });
    }

    /**
     * This method checks the serer to see if credentials exist, if its true it checks in user
     * otherwise it registers user as a new member
     */

    private void ensureLogin(){

        progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        StringRequest postRequest = new StringRequest(Request.Method.POST, UrlConstant.LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response != null) {

                            boolean status = JsonParser.checkStatus(response);

                            if(status){

                                progressDialog.dismiss();

//                                determine if the user if logging in
                                isLogin = true;

                                verifyPaymentStatus();

//                                directToMainPage(JsonParser.getTokenFromLogin(response));
                            }
                            else{
                                isLogin = false;

                                ensureRegistration();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();

                Toast.makeText(getApplication(), "Oops:) something went wrong, please try again...", Toast.LENGTH_LONG).show();
            }
        }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put(Config.USER_EMAIL, email);
                params.put(Config.PASSWORD, password);

                return params;
            }
        };

        queue.add(postRequest);
    }


    private void ensureRegistration (){


        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        StringRequest postRequest = new StringRequest(Request.Method.POST, UrlConstant.REGISTRATION_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response != null) {

                            boolean status = JsonParser.checkStatus(response);

                            progressDialog.dismiss();
                            if(status){

                                verifyPaymentStatus();

//                                directToMainPage(JsonParser.getTokenFromRegistration(response));
                            }
                            else
                                Toast.makeText(PasswordActivity.this, getResources().getString(R.string.error), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();

                Toast.makeText(getApplication(), "Oops:) something went wrong, please try again", Toast.LENGTH_LONG).show();
            }
        }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(Config.USER_EMAIL, email);
                params.put(Config.USER_NAME, username);
                params.put(Config.IMAGE_URL, imageUrl);
                params.put(Config.PASSWORD, password);
                params.put(Config.FIRSTNAME, firstname);
                params.put(Config.LASTNAME, lastname);
                params.put(Config.COUNTRY, country);
                params.put(Config.WHATSAP_NUMBER, "");
                return params;
            }
        };

        queue.add(postRequest);
    }

    private void directToMainPage(String token){

        SharedPreferences sharedPreferences =
                getSharedPreferences(Config.PREFERENCE_KEY, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Config.ACCESS_TOKEN, token);
        editor.putString(Config.USER_EMAIL, email);
        editor.putString(Config.USER_NAME, username);
        editor.putString(Config.IMAGE_URL, imageUrl);
        editor.putString(Config.FIRSTNAME, firstname);
        editor.putString(Config.LASTNAME, lastname);
        editor.putString(Config.COUNTRY, country);
        editor.putBoolean(Config.CONFIRM_PAYMENT, paymentStatus);
        editor.apply();

        SharedPreferences preferences = getSharedPreferences(Config.PREFERENCE_KEY, 0);

        boolean confirmPayment = preferences.getBoolean(Config.CONFIRM_PAYMENT, false);

        if (confirmPayment) {

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();

        } else {

            Intent intent = new Intent(PasswordActivity.this, PayWithPayStackActivity.class);
            startActivity(intent);

        }
    }

    public void onButtonClick(){

        passwordField.setError(null);

        password = passwordField.getText().toString();

        if(TextUtils.isEmpty(password)){
            passwordField.setError(getResources().getString(R.string.password_error));

            focusView = passwordField;

            focusView.requestFocus();

            cancel = true;
        }
        else{
            ensureLogin();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == android.R.id.home){

            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void verifyPaymentStatus(){

        paymentStatusDialog.show();

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        StringRequest postRequest = new StringRequest(Request.Method.POST, UrlConstant.PAYSTACK_VERIFY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response != null) {

                            boolean status = JsonParser.checkStatus(response);

                            paymentStatusDialog.dismiss();

                            paymentStatus = status;
                           if(isLogin){
                               directToMainPage(JsonParser.getTokenFromLogin(response));
                           }
                            else{
                               directToMainPage(JsonParser.getTokenFromRegistration(response));
                           }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                paymentStatusDialog.dismiss();

                Toast.makeText(getApplication(), "Oops:) something went wrong, please try again", Toast.LENGTH_LONG).show();
            }
        }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                SharedPreferences preferences = getSharedPreferences(Config.PREFERENCE_KEY, 0);

                String accessToken = preferences.getString(Config.ACCESS_TOKEN, "");

                Map<String, String> params = new HashMap<>();

                params.put(Config.ACCESS_TOKEN, accessToken);
                return params;
            }
        };

        queue.add(postRequest);
    }
}
