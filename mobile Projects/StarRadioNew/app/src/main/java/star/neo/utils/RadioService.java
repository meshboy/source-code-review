package star.neo.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.widget.Toast;

import star.neo.starradionew.MainActivity;
import star.neo.starradionew.R;


/**
 * Created by mesh on 7/2/16.
 */
public class RadioService extends Service{

    MediaPlayer mPlayer;


    @Override
    public void onCreate() {
        super.onCreate();
//
//        ProgressBarActivity.actionStart(getApplication(), "",
//                getResources().getString(R.string.radio_buffering), false, true);

        Toast.makeText(getApplicationContext(), "Star Radio buffering...", Toast.LENGTH_LONG).show();

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        setUpPlayer();

        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {

                Toast.makeText(getApplicationContext(), "Happy listening...", Toast.LENGTH_SHORT).show();
//                ProgressBarActivity.actionDismiss(getApplication());
                mPlayer.start();

            }
        });

        return START_NOT_STICKY;
    }

    private void showNotification() {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setContentText(getResources().getString(R.string.service_radio));

        Intent resultIntent = new Intent(this, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(0, mBuilder.build());
    }

    public void setUpPlayer() {

        try {

            mPlayer = new MediaPlayer();
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    Toast.makeText(getApplicationContext(), "Oops:) something went wrong... please try again", Toast.LENGTH_LONG).show();
                    return false;
                }
            });
            mPlayer.setDataSource(UrlConstant.LIVE_RADIO_URL);
            mPlayer.prepareAsync();
            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    Toast.makeText(getApplicationContext(), "Thank you for listening to Star Radio", Toast.LENGTH_LONG).show();
                }
            });
            showNotification();
        } catch (Exception e) {

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public  void stop(){

        super.onDestroy();


    }
}
