package star.neo.starradionew;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;


public class SinglePresenterActivity extends AppCompatActivity {

    ActionBar actionBar;

    String name, bio, imageUrl, roleModel;

    ImageLoader mImageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_presenter);


        TextView roleModelView = (TextView) findViewById(R.id.role_model);
        TextView bioView = (TextView) findViewById(R.id.bio);

        ImageView presenterImageView = (ImageView) findViewById(R.id.team_image);

        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        roleModel = intent.getStringExtra("roleModel");
        imageUrl = intent.getStringExtra("imageUrl");
        bio = intent.getStringExtra("bio");

        if(roleModel != null && bio != null){
            roleModelView.setText(roleModel);
            bioView.setText(bio);
        }

        actionBar = getSupportActionBar();

        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(name);
        }

        if(!TextUtils.isEmpty(imageUrl) || imageUrl != null){
//            mImageLoader = MySingleton.getInstance(this)
//                    .getImageLoader();
//
//            mImageLoader.get(imageUrl, ImageLoader.getImageListener(presenterImageView,
//                    R.drawable.userimage, R.drawable.userimage));

            Glide.with(this)
                    .load(imageUrl)
                    .error(R.drawable.userimage)
                    .skipMemoryCache(true)
                    .into(presenterImageView);


        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_single_presenter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
