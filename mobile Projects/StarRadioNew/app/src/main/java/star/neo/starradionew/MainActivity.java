package star.neo.starradionew;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.thefinestartist.finestwebview.FinestWebView;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import star.neo.fragments.AboutFragment;
import star.neo.fragments.AdvertisementFragment;
import star.neo.fragments.ChooseNewsCategoryFragment;
import star.neo.fragments.ContactUs;
import star.neo.fragments.CreatePresenterFragment;
import star.neo.fragments.SettingsFragment;
import star.neo.fragments.TabFragment;
import star.neo.utils.Config;
import star.neo.utils.JsonParser;
import star.neo.utils.MySingleton;
import star.neo.utils.UrlConstant;


public class MainActivity extends AppCompatActivity {

    DrawerLayout drawerLayout;

    Toolbar toolbar;

    ActionBar actionBar;

    FragmentManager mFragmentManager;

    FragmentTransaction mFragmentTransaction;

    TextView userNameView, emailView;

    CircleImageView userImageProfile;

    ImageLoader mImageLoader;

    String imageUrl, username, email;

    ProgressDialog signOutDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.star_radio));


        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();

        if (actionBar != null) {

            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        drawerLayout = (DrawerLayout) findViewById(R.id.navigation_drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        if (navigationView != null) {
            setupNavigationDrawerContent(navigationView);

        }

        setupNavigationDrawerContent(navigationView);

        View header = navigationView.getHeaderView(0);

        userNameView = (TextView) header.findViewById(R.id.username);
        emailView = (TextView) header.findViewById(R.id.email);

        userImageProfile = (CircleImageView) header.findViewById(R.id.image_profile);
        userImageProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ImageActivity.class);
                startActivity(intent);
            }
        });


        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView, new TabFragment()).commit();

        signOutDialog = new ProgressDialog(this);
        signOutDialog.setMessage(getResources().getString(R.string.sign_out_message));


        loadImageProfile();
    }

    private void setupNavigationDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();


                        switch (menuItem.getItemId()) {

                            case R.id.item_navigation_drawer_radio:
                                menuItem.setChecked(true);
                                toolbar.setTitle(getResources().getString(R.string.star_radio));
                                fragmentTransaction.replace(R.id.containerView, new TabFragment()).commit();
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;

                            case R.id.item_navigation_drawer_news:
                                menuItem.setChecked(true);
                                toolbar.setTitle(getResources().getString(R.string.news));
                                fragmentTransaction.replace(R.id.containerView, new ChooseNewsCategoryFragment()).commit();
                                drawerLayout.closeDrawer(GravityCompat.START);

                                return true;

                            case R.id.item_navigation_drawer_place_advert:
                                menuItem.setChecked(true);
                                toolbar.setTitle(getResources().getString(R.string.advert));
//                                Toast.makeText(MainActivity.this, "pending...", Toast.LENGTH_LONG).show();
                                fragmentTransaction.replace(R.id.containerView, new AdvertisementFragment()).commit();
                                drawerLayout.closeDrawer(GravityCompat.START);

                                return true;

                            case R.id.item_navigation_drawer_contact_us:
                                menuItem.setChecked(true);
                                toolbar.setTitle(getResources().getString(R.string.contact_us));
                                fragmentTransaction.replace(R.id.containerView, new ContactUs()).commit();
                                drawerLayout.closeDrawer(GravityCompat.START);

                                return true;

                            case R.id.item_navigation_drawer_share:

                                menuItem.setChecked(true);
                                toolbar.setTitle(getResources().getString(R.string.share));
                                shareApp();
//                                Toast.makeText(MainActivity.this, "pending...", Toast.LENGTH_SHORT).show();
//                                fragmentTransaction.replace(R.id.containerView, new ShareFragment()).commit();
                                drawerLayout.closeDrawer(GravityCompat.START);

                                return true;

                            case R.id.item_navigation_drawer_settings:
                                menuItem.setChecked(true);
                                toolbar.setTitle(getResources().getString(R.string.settings));
                                fragmentTransaction.replace(R.id.containerView, new SettingsFragment()).commit();
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;


                            case R.id.item_navigation_drawer_presenter:
                                menuItem.setChecked(true);
                                toolbar.setTitle(getResources().getString(R.string.presenter));

                                fragmentTransaction.replace(R.id.containerView, new CreatePresenterFragment()).commit();
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;

                            case R.id.item_navigation_drawer_about:

                                menuItem.setChecked(true);
                                toolbar.setTitle(getResources().getString(R.string.about));
                                fragmentTransaction.replace(R.id.containerView, new AboutFragment()).commit();
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;

                            case R.id.item_navigation_drawer_sign_out:
                                menuItem.setChecked(true);
                                signOut();
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;



                        }
                        return true;
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

//        MenuItem menuItem = menu.findItem(R.id.item_navigation_drawer_news);
//
//        getMenuInflater().inflate(R.menu.sub_menu, menuItem.getSubMenu());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                loadImageProfile();
                signInCount();
                return true;

            case R.id.facebook_star:
                launchWeb(UrlConstant.FACEBOOK_SOCIAL, Config.STAR_RADIO_FACEBOOK);
                return true;

            case R.id.instagram_star:
                launchWeb(UrlConstant.INSTAGRAM_SOCIAL, Config.STAR_RADIO_INSTAGRAM);
                return true;

            case R.id.twitter_star:
                launchWeb(UrlConstant.TWITTER_SOCIAL, Config.STAR_RADIO_TWITTER);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    private void loadImageProfile() {

        SharedPreferences preferences = getSharedPreferences(Config.PREFERENCE_KEY, 0);
        imageUrl = preferences.getString(Config.IMAGE_URL, "");
        username = preferences.getString(Config.USER_NAME, "");
        email = preferences.getString(Config.USER_EMAIL, "");

        userNameView.setText(username);
        emailView.setText(email);

        if (imageUrl != null) {

//            Glide.with(this)
//                    .load(imageUrl)
//                    .skipMemoryCache(true)
//                    .placeholder(R.drawable.userimage)
//                    .error(R.drawable.userimage)
//                    .into(userImageProfile);

            mImageLoader = MySingleton.getInstance(this)
                    .getImageLoader();

            mImageLoader.get(imageUrl, ImageLoader.getImageListener(userImageProfile,
                    R.drawable.userimage, R.drawable.userimage));


        }


    }


    public void launchWeb(String url, String title) {

        new FinestWebView.Builder(this)
                .theme(R.style.FinestWebViewTheme)
                .titleDefault(title)
                .toolbarScrollFlags(0)
//                .statusBarColorRes(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark))
//                .toolbarColorRes(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark))
                .titleColorRes(R.color.finestWhite)
//                .urlColorRes(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark))
                .iconDefaultColorRes(R.color.finestWhite)
                .progressBarColorRes(R.color.finestWhite)
//                .swipeRefreshColorRes(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark))
                .menuSelector(R.drawable.selector_light_theme)
                .menuTextGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT)
                .menuTextPaddingRightRes(R.dimen.defaultMenuTextPaddingLeft)
                .dividerHeight(0)
                .gradientDivider(false)
//                    .setCustomAnimations(R.anim.slide_up, R.anim.hold, R.anim.hold, R.anim.slide_down)
                .setCustomAnimations(R.anim.slide_left_in, R.anim.hold, R.anim.hold, R.anim.slide_right_out)
//                    .setCustomAnimations(R.anim.fade_in_fast, R.anim.fade_out_medium, R.anim.fade_in_medium, R.anim.fade_out_fast)
                .disableIconBack(true)
                .disableIconClose(true)
                .disableIconForward(true)
                .disableIconMenu(true)
                .show(url);

//       Intent intent = new Intent(MainActivity.this, WebViewActivity.class);
//       intent.putExtra("url", url);
//       intent.putExtra("title", title);
//       startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();


    }


    public void shareApp(){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_info));
        sendIntent.setType("text/plain");

        startActivity(sendIntent);
    }


    public void signOut(){

        NotificationManager nMgr = (NotificationManager)
                getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancelAll();


        signOutDialog.show();

        SharedPreferences sharedPreferences =
                getSharedPreferences(Config.PREFERENCE_KEY, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Config.ACCESS_TOKEN, "");
        editor.apply();

        RequestQueue queue = Volley.newRequestQueue(getApplication());

        StringRequest postRequest = new StringRequest(Request.Method.POST, UrlConstant.LOG_OUT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response != null) {

                            signOutDialog.dismiss();

                            boolean status = JsonParser.checkStatus(response);

                            Log.d("status", String.valueOf(status));

                            Intent intent = new Intent(MainActivity.this, CreateAccess.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                signOutDialog.dismiss();

                Toast.makeText(MainActivity.this, "Oops:) Your internet is sick. please try again...", Toast.LENGTH_LONG).show();
            }
        }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                SharedPreferences preferences = getSharedPreferences(Config.PREFERENCE_KEY, 0);

                String accessToken = preferences.getString(Config.ACCESS_TOKEN, "");

                Map<String, String> params = new HashMap<>();
                params.put(Config.ACCESS_TOKEN,  accessToken);

                return params;
            }
        };

        queue.add(postRequest);

    }

    public void signInCount(){

        SharedPreferences preferences = getSharedPreferences(Config.PREFERENCE_KEY, 0);

        boolean loginCount = preferences.getBoolean(Config.LOGIN_COUNT, false);

        if(!loginCount){

            RequestQueue queue = Volley.newRequestQueue(getApplication());

            StringRequest postRequest = new StringRequest(Request.Method.POST, UrlConstant.LOGIN_COUNT_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            if (response != null) {

                                boolean status = JsonParser.checkStatus(response);

                                if(status){
                                    SharedPreferences sharedPreferences =
                                            getSharedPreferences(Config.PREFERENCE_KEY, Context.MODE_PRIVATE);

                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putBoolean(Config.LOGIN_COUNT, true);
                                    editor.apply();
                                }
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                }
            }

            ) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    SharedPreferences preferences = getSharedPreferences(Config.PREFERENCE_KEY, 0);

                    String email = preferences.getString(Config.USER_EMAIL, "");

                    Map<String, String> params = new HashMap<>();
                    params.put(Config.USER_EMAIL,  email);

                    return params;
                }
            };

            queue.add(postRequest);
        }

    }
}
