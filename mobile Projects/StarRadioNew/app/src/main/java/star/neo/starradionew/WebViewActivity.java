package star.neo.starradionew;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

public class WebViewActivity extends AppCompatActivity {


    ActionBar actionBar;

    String title, url;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        
        Intent intent = getIntent(); 
        url = intent.getStringExtra("url");
        title = intent.getStringExtra("title");

        actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(title);
        }


    }


}
