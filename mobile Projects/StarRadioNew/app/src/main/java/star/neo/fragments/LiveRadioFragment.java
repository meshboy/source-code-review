package star.neo.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.vstechlab.easyfonts.EasyFonts;

import java.util.List;

import star.neo.models.SchedulesModel;
import star.neo.starradionew.R;
import star.neo.utils.Config;
import star.neo.utils.JsonParser;
import star.neo.utils.MySingleton;
import star.neo.utils.RadioService;
import star.neo.utils.UrlConstant;

public class LiveRadioFragment extends Fragment{

    ImageButton playButton, stopButton;

    ImageView clipArtImageView;

    ProgressDialog progressDialog;

    ProgressBar progressBar;


    String featuredName, featuredUrl;

    List<SchedulesModel> schedulesModelList;

    View view;

    TextView featuringView, featuredNameView;

    boolean isPlaying;

    SharedPreferences preferences;

    ImageLoader mImageLoader;

    public LiveRadioFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_live_radio, container, false);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.radio_buffering));

        progressBar = (ProgressBar) view.findViewById(R.id.progress);

        clipArtImageView = (ImageView) view.findViewById(R.id.clip_art);

        playButton = (ImageButton) view.findViewById(R.id.play_button);
        stopButton = (ImageButton) view.findViewById(R.id.pause_button);

        featuringView = (TextView) view.findViewById(R.id.featuring);
//        featuringView.setVisibility(View.INVISIBLE);
        featuringView.setTypeface(EasyFonts.tangerineBold(getActivity()));

        featuredNameView = (TextView) view.findViewById(R.id.featured_name);
//        featuredNameView.setVisibility(View.INVISIBLE);


//        if(!TextUtils.isEmpty(featuredName)){
//            featuredNameView.setVisibility(View.VISIBLE);
//            featuringView.setVisibility(View.VISIBLE);
//        }

        preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);

        String featuredName = preferences.getString(Config.FEATURE_STATUS, "");
        String urlFeature = preferences.getString(Config.FEATURE_IMAGE_URL, "");

        if(!TextUtils.isEmpty(urlFeature)){

            Glide.with(getActivity())
                    .load(urlFeature)
                    .skipMemoryCache(true)
                    .override(300,300)
                    .placeholder(R.drawable.default_art)
                    .into(clipArtImageView);

            featuredNameView.setVisibility(View.VISIBLE);
            featuringView.setVisibility(View.VISIBLE);

            featuredNameView.setText(featuredName);
        }


        isPlaying = preferences.getBoolean(Config.RADIO_STATE, false);


        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loadSchedules();

                isPlaying = true;

                SharedPreferences.Editor e = preferences.edit();

                e.putBoolean(Config.RADIO_STATE, isPlaying);

                //  Apply changes
                e.apply();

                changeStage(true);

                getActivity().startService(new Intent(getActivity(), RadioService.class));
            }
        });


        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isPlaying = false;

                SharedPreferences.Editor e = preferences.edit();

                e.putBoolean(Config.RADIO_STATE, isPlaying);

                //  Apply changes
                e.apply();

                changeStage(false);

                getActivity().stopService(new Intent(getActivity(), RadioService.class));
            }
        });

        changeStage(isPlaying);

        return view;
    }

    /**
     * @param isPlaying inter-switch button regarding the states
     */
    public void changeStage(boolean isPlaying) {

        if (isPlaying) {
            playButton.setVisibility(View.GONE);
            stopButton.setVisibility(View.VISIBLE);
        } else {
            playButton.setVisibility(View.VISIBLE);
            stopButton.setVisibility(View.GONE);
        }
    }

    public void loadSchedules(){

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest strReq = new StringRequest(Request.Method.GET,
                UrlConstant.SHEDULE_URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (response != null) {

//                   cache fresh comments
                    try {

                        schedulesModelList = JsonParser.parseSchedule(response);

                        for(int i = 0; i < schedulesModelList.size(); i++){

                            if(schedulesModelList.get(i).isScheduleStatus()){

                                featuredName = schedulesModelList.get(i).getScheduleFeaturedName();
                                featuredNameView.setText(featuredName);

                                featuredUrl = schedulesModelList.get(i).getScheduleImageClipArtUrl();

                                Glide.with(getActivity())
                                        .load(featuredUrl)
                                        .skipMemoryCache(true)
                                        .override(300,300)
                                        .placeholder(R.drawable.default_art)
                                        .into(clipArtImageView);

                                featuredNameView.setVisibility(View.VISIBLE);
                                featuringView.setVisibility(View.VISIBLE);

                                SharedPreferences sharedPreferences =
                                        getActivity().getSharedPreferences(Config.PREFERENCE_KEY, Context.MODE_PRIVATE);

                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString(Config.FEATURE_IMAGE_URL, featuredUrl);
                                editor.putString(Config.FEATURE_STATUS, featuredName);
                                editor.apply();

                                break;
                            }
                        }
                    } catch (Exception ex) {

                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        //Adding request to request queue
        queue.add(strReq);

    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        SharedPreferences.Editor e = preferences.edit();

        e.putBoolean(Config.RADIO_STATE, isPlaying);

        //  Apply changes
        e.apply();
    }

    @Override
    public void onPause() {
        super.onPause();

        SharedPreferences.Editor e = preferences.edit();

        e.putBoolean(Config.RADIO_STATE, isPlaying);

        //  Apply changes
        e.apply();
    }
}


