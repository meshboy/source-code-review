package star.neo.fragments;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gc.materialdesign.views.ButtonRectangle;

import java.util.HashMap;
import java.util.Map;

import star.neo.starradionew.R;
import star.neo.utils.Config;
import star.neo.utils.JsonParser;
import star.neo.utils.UrlConstant;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequestFragment extends Fragment {

    EditText requestView;

    boolean cancel = false;

    View focusView = null;

    ProgressDialog progressDialog;

    CoordinatorLayout coordinatorLayout;

    String messageRequest, accessToken;


    public RequestFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view = inflater.inflate(R.layout.fragment_request, container, false);

        requestView = (EditText) view.findViewById(R.id.message);
        requestView.setHorizontallyScrolling(false);
        requestView.setMaxLines(Integer.MAX_VALUE);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.progress_reqeust));

        coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinatorLayoutRequest);

        SharedPreferences preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);

        accessToken = preferences.getString(Config.ACCESS_TOKEN, "");

        ButtonRectangle sendRequestButton = (ButtonRectangle) view.findViewById(R.id.submitRequest);
        sendRequestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                sendRequest();
            }
        });

        return view;
    }

    public void sendRequest(){

        requestView.setError(null);

        messageRequest = requestView.getText().toString();

        if(TextUtils.isEmpty(messageRequest)){

            requestView.setError(getResources().getString(R.string.request_error));

            focusView = requestView;

            focusView.requestFocus();

            cancel = true;
        }else{
            progressDialog.show();



            RequestQueue queue = Volley.newRequestQueue(getActivity());

            StringRequest postRequest = new StringRequest(Request.Method.POST, UrlConstant.ADD_REQUEST,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            if (response != null) {

                                requestView.setText("");
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), JsonParser.parseFeedbackMessage(response), Toast.LENGTH_LONG).show();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.dismiss();

                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "check your network connection", Snackbar.LENGTH_LONG);

                    // Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();

                }
            }

            ) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Log.d("token", accessToken);

                    Map<String, String> params = new HashMap<>();
                    params.put(Config.REQUEST_PARAM, messageRequest);
                    params.put(Config.ACCESS_TOKEN,  accessToken);

                    return params;
                }
            };

            queue.add(postRequest);
        }
    }
}
