package star.neo.starradionew;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import cz.msebera.android.httpclient.Header;
import star.neo.models.ImageModel;
import star.neo.utils.Config;
import star.neo.utils.JsonParser;
import star.neo.utils.MySingleton;
import star.neo.utils.UrlConstant;


public class ImageActivity extends AppCompatActivity {

    ActionBar actionBar;

    String imageUrl, username, accessToken;

    ImageView imageView;

    int PICK_IMAGE_REQUEST = 1;

    Bitmap imageBitmap = null;

    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        SharedPreferences preferences = getSharedPreferences(Config.PREFERENCE_KEY, 0);
        imageUrl = preferences.getString(Config.IMAGE_URL, "");
        username = preferences.getString(Config.USER_NAME, "");
        accessToken = preferences.getString(Config.ACCESS_TOKEN, "");

        progressBar = (ProgressBar) findViewById(R.id.progress);

        actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle(username);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        imageView = (ImageView) findViewById(R.id.my_image_profile);

        Glide.with(this)
                .load(imageUrl)
                .error(R.drawable.userimage)
                .skipMemoryCache(true)
                .into(imageView);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {

            finish();
            return true;
        } else if (id == R.id.pick_image) {

            chooseImageFile();

        }

        return super.onOptionsItemSelected(item);
    }

    private void chooseImageFile() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();

            uploadFile(getFilePath(filePath));

            try {
                //Getting the Bitmap from Gallery
                imageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
                imageView.setImageBitmap(imageBitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void uploadFile(String filePath) {

        progressBar.setVisibility(View.VISIBLE);

        RequestParams params = new RequestParams();
        params.put(Config.ACCESS_TOKEN, accessToken);
        try {
            params.put(Config.IMAGE_URL, new File(filePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClient();

        client.post(UrlConstant.UPDATE_IMAGE_URL, params, new BaseJsonHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, String response, Object o) {

                if(response != null){

                    ImageModel imageModel = JsonParser.parseUserImageProfile(response);

                    SharedPreferences preferences = getSharedPreferences(Config.PREFERENCE_KEY, 0);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(Config.IMAGE_URL, imageModel.getImageUrl());
                    editor.apply();

                    Log.d("imageurl", imageModel.getImageUrl());

                    final String message = imageModel.getFeedback();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            progressBar.setVisibility(View.GONE);

                            Toast.makeText(ImageActivity.this, message, Toast.LENGTH_LONG).show();
                        }
                    });

                }
            }

            @Override
            public void onFailure(int i, Header[] headers, Throwable throwable, String response, Object o) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        progressBar.setVisibility(View.GONE);
                    }
                });

            }

            @Override
            protected Object parseResponse(String s, boolean b) throws Throwable {
                return null;
            }
        });

    }

    private String getFilePath(Uri uri) {

        String[] projection = {MediaStore.Images.Media.DATA};

        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);

        if (cursor == null) return null;

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

        cursor.moveToFirst();

        String s = cursor.getString(column_index);

        cursor.close();
        return s;
    }


}
