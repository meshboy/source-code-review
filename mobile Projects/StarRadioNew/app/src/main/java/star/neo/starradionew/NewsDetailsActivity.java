package star.neo.starradionew;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;

import star.neo.utils.MySingleton;


public class NewsDetailsActivity extends AppCompatActivity {

    ActionBar actionBar;

    String title, content, timeCreated, author, imageUrl, category, link;

    TextView authorHeaderView, authorView, titleView, datePublishedView, contentView;

    ProgressBar progressBar;

    ImageView newsImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);


        authorHeaderView = (TextView) findViewById(R.id.author_header);
        authorView = (TextView) findViewById(R.id.author);
        titleView = (TextView) findViewById(R.id.title);
        datePublishedView = (TextView) findViewById(R.id.time_created);
        contentView = (TextView) findViewById(R.id.content);

        progressBar = (ProgressBar) findViewById(R.id.progress);

        actionBar = getSupportActionBar();

        Intent intent = getIntent();
        title = intent.getStringExtra("title");
        content = intent.getStringExtra("content");
        timeCreated = intent.getStringExtra("time_created");
        author = intent.getStringExtra("author");
        imageUrl = intent.getStringExtra("image_url");
        category = intent.getStringExtra("category");
        link = intent.getStringExtra("link");

        titleView.setText(title);
        contentView.setText(content);
        datePublishedView.setText(timeCreated);
        authorView.setText(author);

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(category);
        }


        newsImageView = (ImageView) findViewById(R.id.image_view);

        newsImageView.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);

        if(!TextUtils.isEmpty(imageUrl)) {

            newsImageView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);


            ImageLoader mImageLoader;

            mImageLoader = MySingleton.getInstance(this)
                    .getImageLoader();

            mImageLoader.get(imageUrl, ImageLoader.getImageListener(newsImageView,
                    R.drawable.dummyimage, R.drawable.dummyimage));

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_news_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
