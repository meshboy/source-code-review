package star.neo.starradionew;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.TextView;
import com.vstechlab.easyfonts.EasyFonts;

import java.util.Random;

import star.neo.utils.Config;


public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        TextView textView = (TextView) findViewById(R.id.textview);
        textView.setTypeface(EasyFonts.tangerineBold(this));

        Random random = new Random();
        int randomNumber = (int)(random.nextDouble() );

        TextView splashMessage = (TextView) findViewById(R.id.splash_message);
        splashMessage.setText(splashMessage(randomNumber));


        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    Thread.sleep(3 * 1000);

                    SharedPreferences preferences = getSharedPreferences(Config.PREFERENCE_KEY, 0);

                    String accessToken = preferences.getString(Config.ACCESS_TOKEN, "");

                    boolean confirmPayment = preferences.getBoolean(Config.CONFIRM_PAYMENT, false);

                    if (!TextUtils.isEmpty(accessToken)) {

                        if(confirmPayment){

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                            finish();

                        }else{
                            Intent intent = new Intent(getApplicationContext(), PayWithPayStackActivity.class);
                            startActivity(intent);
                            finish();
                        }

                    } else {

                        Intent intent = new Intent(getApplicationContext(), CreateAccess.class);
                        startActivity(intent);
                        finish();
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                    finish();
                }
            }
        });

        thread.start();
    }

    public String splashMessage(int integer){

        String result;

        switch(integer){

            case 0:
                result = getResources().getString(R.string.splash_message);
                break;

            case 1:
                result = getResources().getString(R.string.splash_message1);
                break;

            default:
                result = getResources().getString(R.string.splash_message);
        }

        return result;
    }
}
