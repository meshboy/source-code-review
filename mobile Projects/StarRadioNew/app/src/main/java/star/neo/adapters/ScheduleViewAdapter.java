package star.neo.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.volley.toolbox.ImageLoader;
import java.util.Collections;
import java.util.List;

import star.neo.models.SchedulesModel;
import star.neo.starradionew.R;
import star.neo.utils.Config;
import star.neo.utils.DateFormatter;
import star.neo.utils.MySingleton;


/**
 * Created by mesh on 7/5/16.
 */
public class ScheduleViewAdapter extends RecyclerView.Adapter<ScheduleViewHolder> {

    List<SchedulesModel> schedulesModelList = Collections.emptyList();
    Context context;

    ImageLoader mImageLoader;

    public ScheduleViewAdapter(Context context, List<SchedulesModel> schedulesModelList){

        this.context = context;
        this.schedulesModelList = schedulesModelList;
    }
    @Override
    public ScheduleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_schedule_row, parent, false);

        ScheduleViewHolder viewHolder = new ScheduleViewHolder(view);

        return  viewHolder;
    }

    @Override
    public void onBindViewHolder(ScheduleViewHolder holder, int position) {

        holder.scheduleView.setText(schedulesModelList.get(position).getSchedule());
        holder.toView.setText(DateFormatter.getTimeStamp(schedulesModelList.get(position).getScheduleTimeTo()));
        holder.fromview.setText(DateFormatter.getTimeStamp(schedulesModelList.get(position).getScheduleTimeFrom()));
        holder.featuredNameView.setText(schedulesModelList.get(position).getScheduleFeaturedName());

        if(schedulesModelList.get(position).isScheduleStatus()){

            holder.onlineView.setTextColor(ContextCompat.getColor(context, R.color.md_green_500));

            SharedPreferences sharedPreferences =
                    context.getSharedPreferences(Config.PREFERENCE_KEY, Context.MODE_PRIVATE);

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(Config.SCHEDULE_IMAGE_URL, schedulesModelList.get(position).getScheduleImageClipArtUrl());
            editor.apply();
        }

        mImageLoader = MySingleton.getInstance(context.getApplicationContext())
                .getImageLoader();

        mImageLoader.get(schedulesModelList.get(position).getScheduleImageClipArtUrl(),
                ImageLoader.getImageListener(holder.clipArtImageView, R.drawable.dummyimage, R.drawable.dummyimage));

    }

    @Override
    public int getItemCount() {
        try{
            return schedulesModelList.size();
        }
        catch(Exception ex){

            ex.printStackTrace();
            return 0;
        }
    }
}
