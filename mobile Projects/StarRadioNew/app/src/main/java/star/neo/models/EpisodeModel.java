package star.neo.models;

/**
 * Created by mesh on 6/26/16.
 */
public class EpisodeModel  {


    private  String downloadUrl;
    private  String episodeUrl;
    private  String datePublished;
    private String title;
    private String description;
    private int length;
    private  int numberOfDownloads;
    private int numberOfPlayingStream;
    private  int episodeId;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberOfPlayingStream() {
        return numberOfPlayingStream;
    }

    public void setNumberOfPlayingStream(int numberOfPlayingStream) {
        this.numberOfPlayingStream = numberOfPlayingStream;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(String datePublished) {
        this.datePublished = datePublished;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getEpisodeUrl() {
        return episodeUrl;
    }

    public void setEpisodeUrl(String episodeUrl) {
        this.episodeUrl = episodeUrl;
    }

    public int getNumberOfDownloads() {
        return numberOfDownloads;
    }

    public void setNumberOfDownloads(int numberOfDownloads) {
        this.numberOfDownloads = numberOfDownloads;
    }

    public int getEpisodeId() {
        return episodeId;
    }

    public void setEpisodeId(int episodeId) {
        this.episodeId = episodeId;
    }




}
