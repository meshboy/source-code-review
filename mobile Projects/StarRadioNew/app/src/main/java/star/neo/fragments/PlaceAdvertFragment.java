package star.neo.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.gc.materialdesign.views.ButtonRectangle;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import co.paystack.android.Paystack;
import co.paystack.android.PaystackSdk;
import co.paystack.android.model.Card;
import co.paystack.android.model.Charge;
import co.paystack.android.model.Transaction;
import cz.msebera.android.httpclient.Header;
import star.neo.starradionew.R;
import star.neo.utils.Config;
import star.neo.utils.JsonParser;
import star.neo.utils.UrlConstant;
import star.neo.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlaceAdvertFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    boolean isPaystack = false;

    private static final int GROUP_LEN = 4;

    LinearLayout bankDirectLayout, overallPaymentLayout, paystackLayout;

    String bankName, tellerNumber, amountPaid, paymentMethodSelection,
            fileName, extension, filePath, companyBrand, numberOfJingleAir, jingleLength;

    EditText companyBrandText, bankNameText, tellerNumberText, paymentDateText, amountPaidText,
            numberOfJingleAirText, jingleLengthText, expiryMonthText, cardNumberText, cvvText, expiryYearText;

    static String paymentDate;

    View focusView = null;

    ProgressDialog progressDialog, paystatckDialog;

    ArrayAdapter<CharSequence> arrayAdapter;

    TextView fileNameView, notePaystackText;

    int PICK_AUDIO_REQUEST = 1;

    Calendar calendar;
    int year, month, day;

    int koboRate = 100;

    ButtonRectangle paystackButton, submitButton;

    public PlaceAdvertFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        PaystackSdk.initialize(getActivity());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_place_advert, container, false);

        PaystackSdk.setPublicKey(Config.PAYSTACK_PUBLIC_KEY);

        calendar = Calendar.getInstance();

        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        expiryMonthText = (EditText) view.findViewById(R.id.expiry_month);
        cardNumberText = (EditText) view.findViewById(R.id.card_number);
        cvvText = (EditText) view.findViewById(R.id.cvv);
        expiryYearText = (EditText) view.findViewById(R.id.expiry_year);

        cardNumberText.addTextChangedListener(cardWatcher);
        expiryMonthText.addTextChangedListener(new ExpiryWatcher(expiryMonthText));
        expiryYearText.addTextChangedListener(new ExpiryWatcher(expiryYearText));

        bankDirectLayout = (LinearLayout) view.findViewById(R.id.bank_direct);
        overallPaymentLayout = (LinearLayout) view.findViewById(R.id.overall);
        paystackLayout = (LinearLayout) view.findViewById(R.id.paystack_layout);
        overallPaymentLayout.setVisibility(View.GONE);

        fileNameView = (TextView) view.findViewById(R.id.fileName);
        notePaystackText = (TextView) view.findViewById(R.id.payment_advert_note);

        bankNameText = (EditText) view.findViewById(R.id.bank_name);
        tellerNumberText = (EditText) view.findViewById(R.id.teller_numbers);
        paymentDateText = (EditText) view.findViewById(R.id.payment_dates);
        amountPaidText = (EditText) view.findViewById(R.id.amount_paid);
        companyBrandText = (EditText) view.findViewById(R.id.company_brand);
        numberOfJingleAirText = (EditText) view.findViewById(R.id.number_of_jingle_per_air);
        jingleLengthText = (EditText)view.findViewById(R.id.jingle_length);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.progress_reqeust));
        paystatckDialog = new ProgressDialog(getActivity());
        paystatckDialog.setMessage(getResources().getString(R.string.transaction));

        Spinner spinner = (Spinner) view.findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);
        arrayAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.paymentArrayMethod, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);

        SharedPreferences preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);
        String whatsappNumber = preferences.getString(Config.WHATSAP_NUMBER, "");
        if (TextUtils.isEmpty(whatsappNumber)){
            Toast.makeText(getActivity(), "Please update all your profile, under settings before proceeding", Toast.LENGTH_SHORT).show();
        }

        paymentDateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(), "datePicker");

                paymentDateText.setText(paymentDate);
            }
        });

        submitButton = (ButtonRectangle) view.findViewById(R.id.submit);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest();
            }
        });

        view.findViewById(R.id.select_mp3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseJingleFileMp3();
            }
        });

        paystackButton = (ButtonRectangle) view.findViewById(R.id.paystack_payment);

        return view;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        String selectedPayment = parent.getItemAtPosition(position).toString();

        switch (selectedPayment) {

            case "Bank Payment":
                layoutButtons(false);
                paymentMethodSelection = "manual-bank-payment";
                break;

            case "Paystack Payment":
                isPaystack = true;
                layoutButtons(true);
                paymentMethodSelection = "Paystack-Transfer";
                break;

            default:
                overallPaymentLayout.setVisibility(View.GONE);
        }

    }

    private void layoutButtons(boolean isPaystack){

        if(isPaystack){
            overallPaymentLayout.setVisibility(View.VISIBLE);
            bankDirectLayout.setVisibility(View.GONE);
            paystackLayout.setVisibility(View.VISIBLE);
            paystackButton.setVisibility(View.VISIBLE);
            submitButton.setVisibility(View.GONE);
            paystackPayment();


            paystackButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendRequest();
                }
            });
        }
        else{
            overallPaymentLayout.setVisibility(View.VISIBLE);
            bankDirectLayout.setVisibility(View.VISIBLE);
            paystackLayout.setVisibility(View.GONE);
            paystackButton.setVisibility(View.GONE);
            submitButton.setVisibility(View.VISIBLE);
        }
    }


    private void paystackPayment(){

       amountPaidText.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence s, int start, int count, int after) {

           }

           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {

           }

           @Override
           public void afterTextChanged(Editable s) {

              if(s.toString().length() > 0){
                  StringBuffer noteBuffer, buttonBufer;

                  noteBuffer = new StringBuffer();
                  buttonBufer = new StringBuffer();
//
//                  noteBuffer.append("Beware that an amount of N ").append(s.toString()).append(" will be deducted from your account.");
//
//                  buttonBufer.append("Pay N").append(s.toString()).append(" using PayStack");
//
//                  Log.d("checkMeBoy" , noteBuffer.toString());
//
//                  notePaystackText.setText(noteBuffer.toString());
////
//                  paystackButton.setText(buttonBufer.toString());
              }
           }
       });
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void sendRequest() {

        paymentDateText.setError(null);
        amountPaidText.setError(null);

        bankName = bankNameText.getText().toString();
        tellerNumber = tellerNumberText.getText().toString();
        paymentDate = paymentDateText.getText().toString();
        amountPaid = amountPaidText.getText().toString();
        jingleLength = jingleLengthText.getText().toString();
        numberOfJingleAir = numberOfJingleAirText.getText().toString();
        companyBrand = companyBrandText.getText().toString();

        SharedPreferences preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);
        String whatsappNumber = preferences.getString(Config.WHATSAP_NUMBER, "");

        if (TextUtils.isEmpty(companyBrand)) {

            companyBrandText.setError(getResources().getString(R.string.field_error));

            focusView = companyBrandText;
            focusView.requestFocus();

        } else if (TextUtils.isEmpty(paymentDate)) {

            paymentDateText.setError(getResources().getString(R.string.field_error));

            focusView = paymentDateText;
            focusView.requestFocus();

        } else if (TextUtils.isEmpty(amountPaid)) {

            amountPaidText.setError(getResources().getString(R.string.field_error));

            focusView = amountPaidText;
            focusView.requestFocus();
        }else if((Integer.parseInt(amountPaid.trim()) < 100 && paymentMethodSelection.equals("Paystack-Transfer"))){

            amountPaidText.setError(getResources().getString(R.string.amount_error));

            focusView = amountPaidText;
            focusView.requestFocus();
        }else if (TextUtils.isEmpty(whatsappNumber)){
            Toast.makeText(getActivity(), "Please update all your profile, under settings before proceeding", Toast.LENGTH_SHORT).show();
        }
        else{

//            paymentDateText.setError(null);
//            amountPaidText.setError(null);
              if(isPaystack){
                  if (TextUtils.isEmpty(cardNumberText.getText().toString())) {

                      cardNumberText.setError(getResources().getString(R.string.field_error));

                      focusView = cardNumberText;
                      focusView.requestFocus();

                  }
                  else if (TextUtils.isEmpty(cvvText.getText().toString())){
                      cvvText.setError(getResources().getString(R.string.field_error));

                      focusView = cvvText;
                      focusView.requestFocus();
                  }
                  else if(TextUtils.isEmpty(expiryMonthText.getText().toString())){
                      expiryMonthText.setError(getResources().getString(R.string.field_error));

                      focusView = expiryMonthText;
                      focusView.requestFocus();
                  }
                  else if (TextUtils.isEmpty(expiryYearText.getText().toString())){
                      expiryYearText.setError(getResources().getString(R.string.field_error));

                      focusView = expiryYearText;
                      focusView.requestFocus();
                  }
                  else{
                      Log.d("values", cardNumberText.getText().toString());
                      paystatckDialog.show();

                      //validate fields
                      //String number, Integer expiryMonth, Integer expiryYear, String cvc
                      try{

                          String emailAddress = preferences.getString(Config.USER_EMAIL, "");

                          String number = Utils.cleanNumber(cardNumberText.getText().toString().trim());
                          int expiryMonth = Integer.parseInt(expiryMonthText.getText().toString().trim());
                          int expiryYear = Integer.parseInt(expiryYearText.getText().toString().trim());
                          String cvv = cvvText.getText().toString().trim();

                          //create a charge
                          Charge charge = new Charge();
                          //Add card to the charge
                          charge.setCard(new Card.Builder(number, expiryMonth, expiryYear, cvv).build());
                          //add an email for customer
                          charge.setEmail(emailAddress);
                          //add amount to charge
                          int amount = Integer.parseInt(amountPaid.trim());
                          charge.setAmount(amount * koboRate);

                          //charge card
                          PaystackSdk.chargeCard(getActivity(), charge, new Paystack.TransactionCallback() {
                              @Override
                              public void onSuccess(Transaction transaction) {
                                  // This is called only after transaction is deemed successful
                                  // retrieve the transaction, and send its reference to your server
                                  // for verification.

                                  Toast.makeText(getActivity(), "Transaction successful",  Toast.LENGTH_LONG).show();

                                  getAllParamsForAdvertisement();
                              }

                              @Override
                              public void beforeValidate(Transaction transaction) {
                                  // This is called only before requesting OTP
                                  // Save reference so you may send to server. If
                                  // error occurs with OTP, you should still verify on server

                                  paystatckDialog.dismiss();
                              }

                              @Override
                              public void onError(Throwable error) {
                                  //handle error here
                                  paystatckDialog.dismiss();

                                  Toast.makeText(getActivity(), error.getMessage() , Toast.LENGTH_SHORT).show();
                              }

                          });
                      } catch (Exception e) {
                          e.printStackTrace();
                          paystatckDialog.dismiss();
                          Toast.makeText(getActivity(),
                                  "Oops:) please ensure all your card credentials are properfly filled", Toast.LENGTH_SHORT).show();
                      }
                  }

              }
            else{
                  getAllParamsForAdvertisement();
            }
        }

    }


    private void getAllParamsForAdvertisement() {

        progressDialog.show();

        SharedPreferences preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);

        String accessToken = preferences.getString(Config.ACCESS_TOKEN, "");


        RequestParams params = new RequestParams();
        params.put(Config.ACCESS_TOKEN, accessToken);

        if (filePath != null) {
            try {
                params.put(Config.JUNGLE_FILE_NAME, new File(filePath));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        params.put(Config.ACCESS_TOKEN, preferences.getString(Config.ACCESS_TOKEN, ""));
        params.put(Config.PAYMENT_AMOUNT, amountPaid);
        params.put(Config.PAYMENT_DATE, paymentDate);
        params.put(Config.PAYMENT_METHOD, paymentMethodSelection);
        params.put(Config.TELLER_NUMBER, tellerNumber);
        params.put(Config.BANK_NAME, bankName);
        params.put(Config.COMPANY_BRAND, companyBrand);
        params.put(Config.JINGLE_LENGTH, jingleLength);
        params.put(Config.NUMBER_OF_AIR_PER_JINGLE, numberOfJingleAir);

        AsyncHttpClient client = new AsyncHttpClient();

        client.post(UrlConstant.ADVERTISEMENT_URL, params, new BaseJsonHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, final String response, Object o) {

                if (response != null) {


                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            progressDialog.dismiss();

                            Toast.makeText(getActivity(), JsonParser.parseFeedbackMessage(response), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }

            @Override
            public void onFailure(int i, Header[] headers, Throwable throwable, String response, Object o) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        progressDialog.dismiss();

                        Toast.makeText(getActivity(), "please check your internet connection", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            protected Object parseResponse(String s, boolean b) throws Throwable {
                return null;
            }
        });

    }

    public void chooseJingleFileMp3() {

        Intent mediaIntent = new Intent(Intent.ACTION_GET_CONTENT);
        mediaIntent.setType("audio/*"); //set mime type as per requirement
        startActivityForResult(mediaIntent, PICK_AUDIO_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_AUDIO_REQUEST && resultCode == getActivity().RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            filePath = getFilePath(uri);

            File file = new File(getFilePath(uri));

            fileName = file.getName();

            fileNameView.setText(fileName);

            extension = fileName.substring(fileName.lastIndexOf(".") + 1);
            extension = extension.toLowerCase().trim();

            if (!extension.equals("mp3"))
                Toast.makeText(getActivity(), "File not supported. Only mp3 files are allowed", Toast.LENGTH_LONG).show();
        }
    }

    private String getFilePath(Uri uri) {

        String[] projection = {MediaStore.Images.Media.DATA};

        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);

        if (cursor == null) return null;

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

        cursor.moveToFirst();

        String s = cursor.getString(column_index);

        cursor.close();
        return s;
    }


    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            paymentDate = String.valueOf(year) + "/" + (month + 1) + "/" + day;

        }
    }


    /**
     * Text Watcher to format card number
     */
    private TextWatcher cardWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            String number = s.toString();

            if (number.length() >= GROUP_LEN) {

                String formatted = Utils.formatForViewing(number, GROUP_LEN);

                if (!number.equalsIgnoreCase(formatted)) {
                    cardNumberText.removeTextChangedListener(cardWatcher);
                    cardNumberText.setText(formatted);
                    cardNumberText.setSelection(formatted.length());
                    cardNumberText.addTextChangedListener(cardWatcher);
                }
            }
        }
    };

    /**
     * Expiry Watcher
     */
    private class ExpiryWatcher implements TextWatcher {

        private EditText editText;

        public ExpiryWatcher(EditText editText) {
            this.editText = editText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            try{
                int number = Integer.parseInt(s.toString());
                int length = s.length();
                switch (editText.getId()) {
                    case R.id.expiry_month:
                        if(length == 1) {
                            if(number > 1) {
                                //add a 0 in front
                                setText("0"+number);
                            }
                        } else {
                            if(number > 12) {
                                setText("12");
                            }

                            //request focus on the next field
                            expiryYearText.requestFocus();
                        }
                        break;
                    case R.id.expiry_year:
                        String stringYear = (Calendar.getInstance().get(Calendar.YEAR) + "").substring(2);
                        int currentYear = Integer.parseInt(stringYear);

                        if(length == 1) {
                            int firstDigit = Integer.parseInt(String.valueOf(currentYear).substring(0, length));
                            if(number < firstDigit) {
                                setText(firstDigit+"");
                            }
                        } else {
                            if(number < currentYear){
                                setText(currentYear+"");
                            }

                            cvvText.requestFocus();
                        }
                        break;
                }
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            }
        }

        private void setText(String text) {
            editText.setText(text);
            editText.setSelection(editText.getText().toString().trim().length());
        }
    }
}
