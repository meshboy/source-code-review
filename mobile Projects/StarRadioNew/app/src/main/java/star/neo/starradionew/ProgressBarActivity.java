package star.neo.starradionew;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


public class ProgressBarActivity extends AppCompatActivity {

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();

        progressDialog =  ProgressDialog.show(this, intent.getStringExtra("title"), intent.getStringExtra("message"),
                intent.getBooleanExtra("indeterminate", false), intent.getBooleanExtra("cancel", true));

    }

    @Override
    protected void onNewIntent(Intent intent) {
        progressDialog.dismiss();
        finish();
    }

    public static void actionStart(Context from , String title, String message,
                                   boolean intermediate, boolean cancelable){

        Intent intent = new Intent(from, ProgressBarActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("title", title);
        intent.putExtra("message", message);
        intent.putExtra("indeterminate", intermediate);
        intent.putExtra("cancel", cancelable);
        from.startActivity(intent);
    }

    public static void actionDismiss(Context from){

        Intent intent = new Intent(from, ProgressBarActivity.class);
        intent.setFlags(intent.FLAG_ACTIVITY_NEW_TASK | intent.FLAG_ACTIVITY_SINGLE_TOP);
        from.startActivity(intent);
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//
//        finish();
//    }
}
