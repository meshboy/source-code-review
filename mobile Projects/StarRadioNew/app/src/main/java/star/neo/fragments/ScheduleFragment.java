package star.neo.fragments;


import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.List;

import star.neo.adapters.ScheduleViewAdapter;
import star.neo.models.SchedulesModel;
import star.neo.starradionew.R;
import star.neo.utils.Config;
import star.neo.utils.JsonParser;
import star.neo.utils.UrlConstant;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduleFragment extends Fragment {

    RecyclerView recyclerView;

    ScheduleViewAdapter scheduleViewAdapter;

    List<SchedulesModel> schedulesModelList;

    CoordinatorLayout coordinatorLayout;

    ProgressBar progressBar;

    public ScheduleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_scedule, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);

        coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinatorLayoutSchedule);

        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);

        loadSchedules();

        return view;
    }


    public void loadSchedules(){

        showProgress(true);

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest strReq = new StringRequest(Request.Method.GET,
                UrlConstant.SHEDULE_URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Log.d("response", response);

                if (response != null) {

                    showProgress(false);

//                   cache fresh comments
                    try {

                        SharedPreferences preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(Config.SCHEDULE_RESPONSE, response);
                        editor.apply();

                        processJsonResponse(response);

                    } catch (Exception ex) {


                    }


                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                showProgress(false);

                Snackbar snackbar = Snackbar.make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_LONG)
                        .setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loadSchedules();
                            }
                        });
                snackbar.setActionTextColor(Color.RED);

                // Changing action button text color
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();

//                Toast.makeText(getActivity(), "Oops:) something went wrong. please try again...", Toast.LENGTH_LONG).show();

                try{
                    SharedPreferences preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);
                    String jsonResponse = preferences.getString(Config.SCHEDULE_RESPONSE, "");

                    processJsonResponse(jsonResponse);
                }
                catch(Exception ex){

                }
            }
        });

        //Adding request to request queue
        queue.add(strReq);

    }

    public void processJsonResponse(String response){

        schedulesModelList = JsonParser.parseSchedule(response);

        if(!(schedulesModelList.size() > 0))
            Toast.makeText(getActivity(), "No schedule found", Toast.LENGTH_LONG).show();

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        scheduleViewAdapter = new ScheduleViewAdapter(getActivity(), schedulesModelList);
        recyclerView.setAdapter(scheduleViewAdapter);
    }

    public void showProgress (boolean isShowing){

        if(isShowing){

            progressBar.setVisibility(View.VISIBLE);

        }
        else{
            progressBar.setVisibility(View.GONE);

        }
    }


}
