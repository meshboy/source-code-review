package star.neo.fragments;


import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.List;

import star.neo.adapters.EpisodeAdapter;
import star.neo.models.EpisodeModel;
import star.neo.starradionew.R;
import star.neo.utils.Config;
import star.neo.utils.JsonParser;
import star.neo.utils.UrlConstant;

/**
 * A simple {@link Fragment} subclass.
 */
public class EpisodeFragment extends Fragment {

    RecyclerView recyclerView;

    EpisodeAdapter adapter;

    List<EpisodeModel> episodeModelList;

    CoordinatorLayout coordinatorLayout;

    ProgressBar progressBar;

    public EpisodeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_episode, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);

        coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinatorLayout);

        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);

        loadEpisodes();

        return view;
    }

    public void loadEpisodes() {

        showProgress(true);

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest strReq = new StringRequest(Request.Method.GET,
                UrlConstant.EPISODES_URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {


                if (response != null) {

                    showProgress(false);

//                   cache fresh comments
                    try {

                        SharedPreferences preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(Config.EPISODE_JSON_RESPONSE, response);
                        editor.apply();

                        processJsonResponse(response);

                    } catch (Exception ex) {


                    }


                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                showProgress(false);

                Snackbar snackbar = Snackbar.make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_LONG)
                        .setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loadEpisodes();
                            }
                        });
                snackbar.setActionTextColor(Color.RED);

                // Changing action button text color
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();

                try{
                    SharedPreferences preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);
                    String jsonResponse = preferences.getString(Config.EPISODE_JSON_RESPONSE, "");

                    processJsonResponse(jsonResponse);
                }
                catch(Exception ex){

                }
            }
        });

        //Adding request to request queue
        queue.add(strReq);

    }



    public void processJsonResponse(String response) {

        episodeModelList = JsonParser.parseEpisodes(response);

        if(!(episodeModelList.size() > 0))
            Toast.makeText(getActivity(), "No episode found", Toast.LENGTH_LONG).show();

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new EpisodeAdapter(episodeModelList, getActivity());
        recyclerView.setAdapter(adapter);
    }


    public void showProgress(boolean isShowing) {

        if (isShowing) {

            progressBar.setVisibility(View.VISIBLE);

        } else {
            progressBar.setVisibility(View.GONE);

        }
    }
}
