package star.neo.fragments;


import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.List;

import star.neo.adapters.TeamViewAdapter;
import star.neo.adapters.TeamViewHolder;
import star.neo.models.PresentersTeamModel;
import star.neo.starradionew.R;
import star.neo.starradionew.SinglePresenterActivity;
import star.neo.utils.Config;
import star.neo.utils.JsonParser;
import star.neo.utils.UrlConstant;

/**
 * A simple {@link Fragment} subclass.
 */
public class StarRadioTeamFragment extends Fragment {

    RecyclerView recyclerView;

    List<PresentersTeamModel> presentersTeamModelList;

    ProgressBar progressBar;

    CoordinatorLayout coordinatorLayout;

    TeamViewAdapter adapter;


    public StarRadioTeamFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_star_radio_team, container, false);

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Our Team");

        coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinatorLayoutPresent);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);

        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        
        loadPresenters();

        return view;
    }

    public void loadPresenters(){

        showProgressBar(true);

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest strReq = new StringRequest(Request.Method.GET,
                UrlConstant.APPROVED_PRESENTERS_URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                if (response != null) {

                    showProgressBar(false);

//                   cache fresh news
                    try {
                        SharedPreferences preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(Config.PRESENTER_JSON_RESPONSE, response);
                        editor.apply();
                    } catch (Exception ex) {

                    }

                    processJsonResponse(response);

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                showProgressBar(false);

                try{
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Check your internet connection!", Snackbar.LENGTH_LONG)
                            .setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    loadPresenters();
                                }
                            });
                    snackbar.setActionTextColor(Color.RED);

                    // Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();

                    SharedPreferences preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);
                    String jsonResponse = preferences.getString(Config.PRESENTER_JSON_RESPONSE, "");

                    processJsonResponse(jsonResponse);

                }
                catch(Exception ex){

                }
            }
        });

        //Adding request to request queue
        queue.add(strReq);
    }

    public void processJsonResponse(String response) {


        presentersTeamModelList = JsonParser.parseTeamPresenters(response);

        recyclerView.setLayoutManager(determineScreenSize());

        adapter = new TeamViewAdapter(getActivity(), presentersTeamModelList);

        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(new TeamViewAdapter.RecyclerTouchListener(getActivity(), recyclerView,  new TeamViewHolder.ClickListener() {


            @Override
            public void onClick(View view, int position) {

                Intent intent = new Intent(getActivity(), SinglePresenterActivity.class);
                intent.putExtra("name", presentersTeamModelList.get(position).getUsername());
                intent.putExtra("imageUrl", presentersTeamModelList.get(position).getImageUrl());
                intent.putExtra("roleModel", presentersTeamModelList.get(position).getRoleModel());
                intent.putExtra("bio", presentersTeamModelList.get(position).getBiography());

                startActivity(intent);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));



    }

    public void showProgressBar(boolean isShowing) {

        if (isShowing) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    /**
     *
     * @return the corresponding layout for screen size
     */

    private RecyclerView.LayoutManager determineScreenSize (){

        switch ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)){

            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                return new GridLayoutManager(getActivity(), 4);

            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                return new GridLayoutManager(getActivity(), 3);

            case Configuration.SCREENLAYOUT_SIZE_MASK:
                return new GridLayoutManager(getActivity(), 3);

            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                return new GridLayoutManager(getActivity(), 3);

            default:
                return new GridLayoutManager(getActivity(), 3);
        }

    }

}
