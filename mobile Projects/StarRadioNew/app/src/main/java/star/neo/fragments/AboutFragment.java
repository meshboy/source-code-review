package star.neo.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gc.materialdesign.views.ButtonRectangle;
import com.thefinestartist.finestwebview.FinestWebView;

import star.neo.starradionew.R;
import star.neo.utils.UrlConstant;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends Fragment {


    public AboutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about, container, false);

        ButtonRectangle visitSiteButton = (ButtonRectangle) view.findViewById(R.id.visit);
        visitSiteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        launchWeb(UrlConstant.WEBSITE_URL, getResources().getString(R.string.app_name));
                    }
                });


        return view;
    }

    public void launchWeb(String url, String title) {

        new FinestWebView.Builder(getActivity())
                .theme(R.style.FinestWebViewTheme)
                .titleDefault(title)
                .toolbarScrollFlags(0)
//                .statusBarColorRes(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark))
//                .toolbarColorRes(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark))
                .titleColorRes(R.color.finestWhite)
//                .urlColorRes(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark))
                .iconDefaultColorRes(R.color.finestWhite)
                .progressBarColorRes(R.color.finestWhite)
//                .swipeRefreshColorRes(ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark))
                .menuSelector(R.drawable.selector_light_theme)
                .menuTextGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT)
                .menuTextPaddingRightRes(R.dimen.defaultMenuTextPaddingLeft)
                .dividerHeight(0)
                .gradientDivider(false)
//                    .setCustomAnimations(R.anim.slide_up, R.anim.hold, R.anim.hold, R.anim.slide_down)
                .setCustomAnimations(R.anim.slide_left_in, R.anim.hold, R.anim.hold, R.anim.slide_right_out)
//                    .setCustomAnimations(R.anim.fade_in_fast, R.anim.fade_out_medium, R.anim.fade_in_medium, R.anim.fade_out_fast)
                .disableIconBack(true)
                .disableIconClose(true)
                .disableIconForward(true)
                .disableIconMenu(true)
                .show(url);

//       Intent intent = new Intent(MainActivity.this, WebViewActivity.class);
//       intent.putExtra("url", url);
//       intent.putExtra("title", title);
//       startActivity(intent);
    }



}
