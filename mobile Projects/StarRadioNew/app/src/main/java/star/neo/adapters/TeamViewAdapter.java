package star.neo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.android.volley.toolbox.ImageLoader;
import com.vstechlab.easyfonts.EasyFonts;

import java.util.Collections;
import java.util.List;

import star.neo.models.PresentersTeamModel;
import star.neo.starradionew.R;
import star.neo.utils.MySingleton;

/**
 * Created by mesh on 7/6/16.
 */
public class TeamViewAdapter extends RecyclerView.Adapter<TeamViewHolder> {

    ImageLoader mImageLoader;

    List<PresentersTeamModel> presentersTeamModelList = Collections.emptyList();

    Context context;

    public TeamViewAdapter(Context context, List<PresentersTeamModel> presentersTeamModelList){
        this.presentersTeamModelList = presentersTeamModelList;
        this.context = context;
    }

    @Override
    public TeamViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_starradio_team_row, parent, false);

        TeamViewHolder viewHolder = new TeamViewHolder(view);

        return  viewHolder;
    }

    @Override
    public void onBindViewHolder(TeamViewHolder holder, int position) {

        mImageLoader = MySingleton.getInstance(context.getApplicationContext())
                .getImageLoader();

        String imageUrl = presentersTeamModelList.get(position).getImageUrl();

        if(!TextUtils.isEmpty(imageUrl) || imageUrl != null)
            mImageLoader.get(presentersTeamModelList.get(position).getImageUrl(),
                ImageLoader.getImageListener(holder.presenterImage, R.drawable.userimage, R.drawable.userimage));

        holder.presentersName.setText(presentersTeamModelList.get(position).getUsername());
        holder.presentersName.setTypeface(EasyFonts.tangerineBold(context));
    }

    @Override
    public int getItemCount() {

        try{
            return presentersTeamModelList.size();
        }
        catch(Exception ex){
            return 0;
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class RecyclerTouchListener  implements RecyclerView.OnItemTouchListener{

        private TeamViewHolder.ClickListener clickListener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final TeamViewHolder.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
