package star.neo.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import star.neo.starradionew.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChooseNewsCategoryFragment extends Fragment implements View.OnClickListener {


    public ChooseNewsCategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news_category, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.news));

        LinearLayout politicsLayout = (LinearLayout) view.findViewById(R.id.politics);
        LinearLayout dorogistLayout = (LinearLayout) view.findViewById(R.id.dorogist);
        LinearLayout musicLayout = (LinearLayout) view.findViewById(R.id.music);
        LinearLayout eventLayout = (LinearLayout) view.findViewById(R.id.events);

        politicsLayout.setOnClickListener(this);
        dorogistLayout.setOnClickListener(this);
        musicLayout.setOnClickListener(this);
        eventLayout.setOnClickListener(this);

        return view;
    }


    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {

            case R.id.politics:
                goToNewsPage(getResources().getString(R.string.politics));
                break;

            case R.id.dorogist:
                goToNewsPage(getResources().getString(R.string.dorogist));
                break;

            case R.id.music:
                goToNewsPage(getResources().getString(R.string.music));
                break;

            case R.id.events:
                goToNewsPage(getResources().getString(R.string.events));
                break;
        }

    }

    public void goToNewsPage(String category) {

        Toast.makeText(getActivity(), "Coming soon..", Toast.LENGTH_SHORT).show();

//        Bundle os = new Bundle();
//        os.putString(Config.CATEGORY_BUNDLE, category);
//
//        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.replace(R.id.containerView, NewsFragment.newsInstance(os)).addToBackStack(null).commit();

    }


}
