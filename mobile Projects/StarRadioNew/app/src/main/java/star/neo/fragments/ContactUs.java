package star.neo.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.vstechlab.easyfonts.EasyFonts;

import star.neo.starradionew.R;
import star.neo.utils.Config;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContactUs extends Fragment {


    public ContactUs() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);

        TextView textView = (TextView) view.findViewById(R.id.header_contact);
        textView.setTypeface(EasyFonts.droidSerifBoldItalic(getActivity()));

        view.findViewById(R.id.live_call).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call(Config.LIVE_CONTACT);
            }
        });

        view.findViewById(R.id.advert_call).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call(Config.ADVERT_CONTACT);
            }
        });

        view.findViewById(R.id.general_call).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call(Config.GENERAL_CONTACT);
            }
        });

        return view;
    }


    public void call(String phoneNumber) {

        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        startActivity(intent);
    }

}
