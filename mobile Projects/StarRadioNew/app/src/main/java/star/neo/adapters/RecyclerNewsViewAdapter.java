package star.neo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.android.volley.toolbox.ImageLoader;

import java.util.Collections;
import java.util.List;

import star.neo.models.NewsModel;
import star.neo.starradionew.R;
import star.neo.utils.MySingleton;

/**
 * Created by mesh on 6/1/16.
 */
public class RecyclerNewsViewAdapter extends RecyclerView.Adapter<NewsViewHolder> {

    List<NewsModel> newsModelList = Collections.emptyList();
    Context context;

    ImageLoader mImageLoader;


    public RecyclerNewsViewAdapter(List<NewsModel> newsModelList, Context context) {

        this.newsModelList = newsModelList;
        this.context = context;
    }


    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_news_layout_row, parent, false);

        NewsViewHolder viewHolder = new NewsViewHolder(view);

        return  viewHolder;
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {

        String imageUrl = newsModelList.get(position).getNewsImageUrl();

        mImageLoader = MySingleton.getInstance(context)
                .getImageLoader();
//


        if(imageUrl == null || TextUtils.isEmpty(imageUrl)){
            holder.newsImage.setVisibility(View.GONE);
        }
        else{
            mImageLoader.get(imageUrl, ImageLoader.getImageListener(holder.newsImage,
                    R.drawable.dummyimage, R.drawable.dummyimage));
        }

        holder.newsTitleView.setText(newsModelList.get(position).getNewsTitle());
        holder.newsContentView.setText(newsModelList.get(position).getNewSubContent());
        holder.timeCreatedView.setText(newsModelList.get(position).getNewsTimeCreated());


    }



    @Override
    public int getItemCount() {
        return newsModelList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class RecyclerTouchListener  implements RecyclerView.OnItemTouchListener{

        private NewsViewHolder.ClickListener clickListener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final NewsViewHolder.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
