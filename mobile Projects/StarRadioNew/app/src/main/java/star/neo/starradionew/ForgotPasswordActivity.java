package star.neo.starradionew;

import android.app.ProgressDialog;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.gc.materialdesign.views.ButtonRectangle;

import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;
import star.neo.utils.Config;
import star.neo.utils.JsonParser;
import star.neo.utils.UrlConstant;
import star.neo.utils.Utility;

public class ForgotPasswordActivity extends AppCompatActivity {

    ActionBar actionBar;

    EditText emailText;

    String email;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        Fabric.with(this, new Crashlytics());

        actionBar = getSupportActionBar();

        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        emailText = (EditText) findViewById(R.id.email);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.loading));

        ButtonRectangle forgotPassword = (ButtonRectangle) findViewById(R.id.forgot_passsword);
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest();
            }
        });



    }

    private void sendRequest(){

        email = emailText.getText().toString();
        if(!Utility.isValidEmail(email)){

            Toast.makeText(this, "Please provide a valid email address", Toast.LENGTH_SHORT).show();
        }
        else if(TextUtils.isEmpty(email)){

            Toast.makeText(this, "Please provide your email address", Toast.LENGTH_SHORT).show();
        }
        else{
            progressDialog.show();

            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

            StringRequest postRequest = new StringRequest(Request.Method.POST, UrlConstant.FORGOT_PASSWORD,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            if (response != null) {
                                progressDialog.dismiss();

                                Toast.makeText(ForgotPasswordActivity.this, JsonParser.parseFeedbackMessage(response), Toast.LENGTH_LONG).show();
                            }
                            else
                                Toast.makeText(getApplication(), "Oops:) something went wrong, please try again...", Toast.LENGTH_LONG).show();

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.dismiss();

                    Toast.makeText(getApplication(), "Oops:) something went wrong, please try again...", Toast.LENGTH_LONG).show();
                }
            }

            ) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();
                    params.put(Config.USER_EMAIL, email);


                    return params;
                }
            };

            queue.add(postRequest);

        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_forgot_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
