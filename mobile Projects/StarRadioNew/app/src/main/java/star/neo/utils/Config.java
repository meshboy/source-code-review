package star.neo.utils;

/**
 * Created by mesh on 6/22/16.
 */
public class Config {


    public static String PREFERENCE_KEY = "PREFERENCE_FILE_KEY";

    public static String MP3_FOLDER =  "StarRadio";

//    USER PARAMS
    public static String USER_EMAIL = "email";
    public static String USER_NAME = "username";
    public static String IMAGE_URL = "image_url";
    public static String ACCESS_TOKEN = "google_access_token";
    public static String COUNTRY = "country";
    public static String FIRSTNAME = "firstname";
    public static String LASTNAME = "lastname";
    public static String PASSWORD = "password";
    public static String OLD_PASSWORD = "old_password";
    public static String WHATSAP_NUMBER = "whatsapp_number";
    public static String LOGIN_COUNT = "login_count";

//    COMMENT PARAM
    public static String COMMENT_PARAM = "comments";

//    REQUEST PARAM
    public static String REQUEST_PARAM = "requests";

//    LOCATION PARAM
    public static String LATITUDE = "latitude";
    public static String LONGITUDE = "longitude";

    public static String JSON_RESPONSE = "jsonresponse";

//    GET CATEGORY TYPE
    public static String CATEGORY_BUNDLE = "category";


    public static String COMMENT_JSON_RESPONSE = "jsonresponsecomment";

    public static String SPREAKER_USER_TOKEN = "8954480";

    public static String EPISODE_JSON_RESPONSE = "jsonepisoderesponse";


    public static String RADIO_STREAM_RESPONSE = "starradioplaymode";

    public static String SCHEDULE_RESPONSE = "jsonschedulereponse";

    public static String LIVE_CONTACT = "08135621475";
    public static String GENERAL_CONTACT = "09059685956";
    public static String ADVERT_CONTACT = "08133292219";

//    SHEDULES
    public static String ONLINE_STATUS = "onlinestatus";
    public static String SCHEDULE_IMAGE_URL = "schedulecliparturl";
    public static String SCHEDULE_FEAUTURED_NAME = "featuredname";


//    PRESENTERS
    public static String PRESENTER_BIO = "presenter_bio";
    public static String PRESENTER_ROLE_MODEL = "presenter_role_model";
    public static String PRESENTER_JSON_RESPONSE = "presenter_json_response";


//    SOCIAL MEDIA
    public static String STAR_RADIO_FACEBOOK = "Star Radio @facebook";
    public static String STAR_RADIO_TWITTER = "Star Radio @twitter";
    public static String STAR_RADIO_INSTAGRAM = "Star Radio @instagram";

//    ADVERT PARAMS
    public static String JUNGLE_FILE_NAME = "jingle_file_name";
    public static String PAYMENT_AMOUNT = "payment_amount";
    public static String PAYMENT_DATE = "payment_date";
    public static String PAYMENT_METHOD = "payment_method";
    public static String TELLER_NUMBER = "teller_number";
    public static String BANK_NAME = "bank_name";
    public static String COMPANY_BRAND = "company_brand";
    public static String JINGLE_LENGTH = "jingle_length";
    public static String NUMBER_OF_AIR_PER_JINGLE = "jingle_number_air_per_day";

    public static String RADIO_STATE = "change_state";

//    CONFIRM PAYMENT
    public static String CONFIRM_PAYMENT = "payment_confirmed";
    public static String CARD_TOKEN = "card_token";

    public static String PAYSTACK_PUBLIC_KEY = "pk_live_71632e427985331ff0ce7b7e139348797e61f2ae";
    public static int PAYSTACK_AMOUNT = 10000;

//    LIVE RADIO FEATURES
    public static String FEATURE_IMAGE_URL = "featureImageUrl";
    public static String FEATURE_STATUS = "featureStatus";
}
