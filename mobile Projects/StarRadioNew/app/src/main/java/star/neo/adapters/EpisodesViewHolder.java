package star.neo.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import star.neo.starradionew.R;

/**
 * Created by mesh on 6/26/16.
 */
public class EpisodesViewHolder extends RecyclerView.ViewHolder {

    TextView episodeNameView, episodeDescView, episodeDownloadNumberView, episodeTimeCreated, episodeStreamView;

    CircleImageView episodeImage, downloadButton;

    public EpisodesViewHolder(View itemView) {
        super(itemView);

        episodeNameView = (TextView) itemView.findViewById(R.id.episode_name);
        episodeDescView = (TextView) itemView.findViewById(R.id.episode_desc);
        episodeDownloadNumberView = (TextView) itemView.findViewById(R.id.episode_download);
        episodeTimeCreated = (TextView) itemView.findViewById(R.id.time);
        episodeStreamView = (TextView) itemView.findViewById(R.id.episode_stream);

        episodeImage = (CircleImageView) itemView.findViewById(R.id.episode_url);
        downloadButton = (CircleImageView) itemView.findViewById(R.id.download_button);
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }
}
