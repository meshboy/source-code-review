package star.neo.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import star.neo.starradionew.R;

/**
 * Created by mesh on 7/5/16.
 */
public class ScheduleViewHolder extends RecyclerView.ViewHolder {
    TextView scheduleView, toView, fromview, onlineView, featuredNameView;
    CircleImageView clipArtImageView;

    public ScheduleViewHolder(View itemView) {
        super(itemView);

        scheduleView = (TextView) itemView.findViewById(R.id.schedule);
        toView = (TextView) itemView.findViewById(R.id.schedule_to);
        fromview = (TextView) itemView.findViewById(R.id.schedule_from);
        onlineView = (TextView) itemView.findViewById(R.id.online_status);
        featuredNameView = (TextView) itemView.findViewById(R.id.featured_name);

        clipArtImageView = (CircleImageView) itemView.findViewById(R.id.schedule_url);

    }
}
