package star.neo.starradionew;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gc.materialdesign.views.ButtonRectangle;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import co.paystack.android.Paystack;
import co.paystack.android.PaystackSdk;
import co.paystack.android.model.Card;
import co.paystack.android.model.Charge;
import co.paystack.android.model.Transaction;
import star.neo.utils.Config;
import star.neo.utils.JsonParser;
import star.neo.utils.UrlConstant;
import star.neo.utils.Utils;


public class PayWithPayStackActivity extends AppCompatActivity {

    private static final int GROUP_LEN = 4;

    EditText expiryMonthText, cardNumberText, cvvText, expiryYearText;

    ButtonRectangle paystackButton;

    LinearLayout createTokenLayout, paystackLayout;

    String emailAddress;

    ProgressDialog paymentDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PaystackSdk.initialize(getApplicationContext());
        setContentView(R.layout.activity_pay_with_pay_stack);

        PaystackSdk.setPublicKey(Config.PAYSTACK_PUBLIC_KEY);

        expiryMonthText = (EditText) findViewById(R.id.expiry_month);
        cardNumberText = (EditText) findViewById(R.id.card_number);
        cvvText = (EditText) findViewById(R.id.cvv);
        expiryYearText = (EditText) findViewById(R.id.expiry_year);

        cardNumberText.addTextChangedListener(cardWatcher);
        expiryMonthText.addTextChangedListener(new ExpiryWatcher(expiryMonthText));
        expiryYearText.addTextChangedListener(new ExpiryWatcher(expiryYearText));

        paymentDialog = new ProgressDialog(this);
        paymentDialog.setMessage(getResources().getString(R.string.send_token_dialog));

        SharedPreferences preferences = getSharedPreferences(Config.PREFERENCE_KEY, 0);

        emailAddress = preferences.getString(Config.USER_EMAIL, "");

        paystackButton = (ButtonRectangle) findViewById(R.id.pay);

        paystackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                paymentDialog.show();

                //validate fields
                //String number, Integer expiryMonth, Integer expiryYear, String cvc
                try{
                    String number = Utils.cleanNumber(cardNumberText.getText().toString().trim());
                    int expiryMonth = Integer.parseInt(expiryMonthText.getText().toString().trim());
                    int expiryYear = Integer.parseInt(expiryYearText.getText().toString().trim());
                    String cvv = cvvText.getText().toString().trim();

                    //create a charge
                    Charge charge = new Charge();
                    //Add card to the charge
                    charge.setCard(new Card.Builder(number, expiryMonth, expiryYear, cvv).build());
                    //add an email for customer
                    charge.setEmail(emailAddress);
                    //add amount to charge
                    charge.setAmount(Config.PAYSTACK_AMOUNT);

                    //charge card
                    PaystackSdk.chargeCard(PayWithPayStackActivity.this, charge, new Paystack.TransactionCallback() {
                        @Override
                        public void onSuccess(Transaction transaction) {
                            // This is called only after transaction is deemed successful
                            // retrieve the transaction, and send its reference to your server
                            // for verification.

                            setUpAccount();
                        }

                        @Override
                        public void beforeValidate(Transaction transaction) {
                            // This is called only before requesting OTP
                            // Save reference so you may send to server. If
                            // error occurs with OTP, you should still verify on server

                            paymentDialog.dismiss();
                        }

                        @Override
                        public void onError(Throwable error) {
                            //handle error here
                            paymentDialog.dismiss();

                            Toast.makeText(PayWithPayStackActivity.this,
                                    "Oops:) something went wrong. please try again...", Toast.LENGTH_SHORT).show();
                        }

                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    paymentDialog.dismiss();
                    Toast.makeText(PayWithPayStackActivity.this,
                            "Oops:) something went wrong. please try again...", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    /**
     * Text Watcher to format card number
     */
    private TextWatcher cardWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            String number = s.toString();

            if (number.length() >= GROUP_LEN) {

                String formatted = Utils.formatForViewing(number, GROUP_LEN);

                if (!number.equalsIgnoreCase(formatted)) {
                    cardNumberText.removeTextChangedListener(cardWatcher);
                    cardNumberText.setText(formatted);
                    cardNumberText.setSelection(formatted.length());
                    cardNumberText.addTextChangedListener(cardWatcher);
                }
            }
        }
    };

    /**
     * Expiry Watcher
     */
    private class ExpiryWatcher implements TextWatcher {

        private EditText editText;

        public ExpiryWatcher(EditText editText) {
            this.editText = editText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            try{
                int number = Integer.parseInt(s.toString());
                int length = s.length();
                switch (editText.getId()) {
                    case R.id.expiry_month:
                        if(length == 1) {
                            if(number > 1) {
                                //add a 0 in front
                                setText("0"+number);
                            }
                        } else {
                            if(number > 12) {
                                setText("12");
                            }

                            //request focus on the next field
                            expiryYearText.requestFocus();
                        }
                        break;
                    case R.id.expiry_year:
                        String stringYear = (Calendar.getInstance().get(Calendar.YEAR) + "").substring(2);
                        int currentYear = Integer.parseInt(stringYear);

                        if(length == 1) {
                            int firstDigit = Integer.parseInt(String.valueOf(currentYear).substring(0, length));
                            if(number < firstDigit) {
                                setText(firstDigit+"");
                            }
                        } else {
                            if(number < currentYear){
                                setText(currentYear+"");
                            }

                            cvvText.requestFocus();
                        }
                        break;
                }
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            }
        }

        private void setText(String text) {
            editText.setText(text);
            editText.setSelection(editText.getText().toString().trim().length());
        }
    }


    private void setUpAccount(){

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        StringRequest postRequest = new StringRequest(Request.Method.POST, UrlConstant.ADD_PAYMENT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response != null) {

                            boolean status = JsonParser.checkStatus(response);

                            paymentDialog.dismiss();

                            if(status){

                                SharedPreferences sharedPreferences =
                                        getSharedPreferences(Config.PREFERENCE_KEY, Context.MODE_PRIVATE);

                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putBoolean(Config.CONFIRM_PAYMENT, true);
                                editor.apply();

                                Intent intent = new Intent(PayWithPayStackActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                            else{
                                Toast.makeText(getApplicationContext(), JsonParser.parseFeedbackMessage(response), Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                paymentDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Oops:) something went wrong. please try again...", Toast.LENGTH_LONG).show();
            }
        }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                SharedPreferences preferences = getSharedPreferences(Config.PREFERENCE_KEY, 0);

                String accessToken = preferences.getString(Config.ACCESS_TOKEN, "");

                Map<String, String> params = new HashMap<>();
                params.put(Config.ACCESS_TOKEN, accessToken);

                return params;
            }
        };

        queue.add(postRequest);
    }
}
