package star.neo.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import star.neo.starradionew.R;


/**
 * Created by mesh on 6/1/16.
 */
public class NewsViewHolder extends RecyclerView.ViewHolder {

    TextView timeCreatedView, newsTitleView, newsContentView;

    ImageView newsImage;

    CardView cardView;

    public NewsViewHolder(View itemView) {
        super(itemView);


        timeCreatedView = (TextView) itemView.findViewById(R.id.time_created);
        newsTitleView = (TextView) itemView.findViewById(R.id.news_title);
        newsContentView = (TextView) itemView.findViewById(R.id.news_content);

        cardView = (CardView) itemView.findViewById(R.id.cardView);

        newsImage = (ImageView) itemView.findViewById(R.id.news_image);
    }


    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }
}
