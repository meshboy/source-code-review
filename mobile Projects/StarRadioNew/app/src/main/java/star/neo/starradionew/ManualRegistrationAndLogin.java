package star.neo.starradionew;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.gc.materialdesign.views.ButtonRectangle;
import com.github.florent37.materialtextfield.MaterialTextField;
import com.vstechlab.easyfonts.EasyFonts;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.fabric.sdk.android.Fabric;
import star.neo.models.UserModel;
import star.neo.utils.Config;
import star.neo.utils.JsonParser;
import star.neo.utils.UrlConstant;
import star.neo.utils.Utility;


public class ManualRegistrationAndLogin extends AppCompatActivity implements View.OnClickListener {

    TextView textView;

    ProgressDialog progressDialogReg, progressDialogLogin, paymentStatusDialog;

    ButtonRectangle signInButton, registrationButton;

    EditText usernameView, passwordView, emailView;

    LinearLayout loginLayout, registrationLayout;

    String username, password, email;

    View focusView = null;
    boolean cancel = false;

    ActionBar actionBar;

    MaterialTextField usernameViewField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_registration_and_login);

        actionBar = getSupportActionBar();

        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Fabric.with(this, new Crashlytics());


        usernameViewField = (MaterialTextField) findViewById(R.id.username_);

        textView = (TextView) findViewById(R.id.textview);
        textView.setTypeface(EasyFonts.robotoBoldItalic(this));

        SpannableString spannableStringReg =  new SpannableString(getResources().getString(R.string.loading_registering));
        spannableStringReg.setSpan(new ForegroundColorSpan(Color.BLACK), 0, spannableStringReg.length(), 0);

        progressDialogReg = new ProgressDialog(this);
        progressDialogReg.setMessage(spannableStringReg);

        SpannableString spannableStringLogin =  new SpannableString(getResources().getString(R.string.loading_sigin));
        spannableStringLogin.setSpan(new ForegroundColorSpan(Color.BLACK), 0, spannableStringLogin.length(), 0);

        progressDialogLogin = new ProgressDialog(this);
        progressDialogLogin.setMessage(spannableStringLogin);

        SpannableString spannableString1 =  new SpannableString(getResources().getString(R.string.verify_payment));
        spannableString1.setSpan(new ForegroundColorSpan(Color.BLACK), 0, spannableString1.length(), 0);

        paymentStatusDialog = new ProgressDialog(this);
        paymentStatusDialog.setMessage(spannableString1);

        usernameView = (EditText) findViewById(R.id.username);
        passwordView = (EditText) findViewById(R.id.password);
        emailView = (EditText) findViewById(R.id.email);

        loginLayout = (LinearLayout) findViewById(R.id.login);
        registrationLayout = (LinearLayout) findViewById(R.id.registration);

        signInButton = (ButtonRectangle) findViewById(R.id.loginButton);
        signInButton.setOnClickListener(this);
        registrationButton = (ButtonRectangle) findViewById(R.id.getStartedButton);
        registrationButton.setOnClickListener(this);

        findViewById(R.id.change_to_sign_in).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeView(true);
            }
        });

        findViewById(R.id.change_to_reg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeView(false);
            }
        });


        findViewById(R.id.forgot_passsword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManualRegistrationAndLogin.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
    }

    private void changeView (boolean login){

        if(login){

            usernameViewField.setVisibility(View.GONE);
            loginLayout.setVisibility(View.VISIBLE);
            registrationLayout.setVisibility(View.GONE);

        }
        else{
            usernameViewField.setVisibility(View.VISIBLE);
            loginLayout.setVisibility(View.GONE);
            registrationLayout.setVisibility(View.VISIBLE);
        }
    }

    private void signIn(){

        progressDialogLogin.show();

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        StringRequest postRequest = new StringRequest(Request.Method.POST, UrlConstant.LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response != null) {
                            progressDialogLogin.dismiss();

                            boolean status = JsonParser.checkStatus(response);

                            if(status){
                                List<UserModel> userModelList = JsonParser.parseManualLogin(response);

                                if(userModelList != null){

                                    UserModel userModel = userModelList.get(0);

                                        SharedPreferences sharedPreferences =
                                                getSharedPreferences(Config.PREFERENCE_KEY, Context.MODE_PRIVATE);

                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.putString(Config.ACCESS_TOKEN, userModel.getAccessToken());
                                    editor.putString(Config.USER_EMAIL, userModel.getEmail());
                                    editor.putString(Config.USER_NAME, userModel.getUsername());
                                    editor.putString(Config.IMAGE_URL, userModel.getImageUrl());
                                    editor.putString(Config.FIRSTNAME, userModel.getFirstname());
                                    editor.putString(Config.LASTNAME, userModel.getLastname());
                                    editor.putString(Config.COUNTRY, userModel.getCountry());
                                    editor.apply();

                                    SharedPreferences preferences = getSharedPreferences(Config.PREFERENCE_KEY, 0);

                                    boolean confirmPayment = preferences.getBoolean(Config.CONFIRM_PAYMENT, false);

                                    if(confirmPayment){
                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        startActivity(intent);
                                        finish();

                                    }else{
                                        verifyPaymentStatus();

                                    }

                                }
                                else
                                    Toast.makeText(getApplication(), "Oops:) something went wrong, please try again...", Toast.LENGTH_LONG).show();
                            }
                            else
                                Toast.makeText(ManualRegistrationAndLogin.this, JsonParser.parseFeedbackMessage(response), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialogLogin.dismiss();

                Toast.makeText(getApplication(), "Oops:) something went wrong, please try again...", Toast.LENGTH_LONG).show();
            }
        }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put(Config.USER_EMAIL, email);
                params.put(Config.PASSWORD, password);

                return params;
            }
        };

        queue.add(postRequest);
    }

    private void registration(){

        progressDialogReg.show();

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        StringRequest postRequest = new StringRequest(Request.Method.POST, UrlConstant.REGISTRATION_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressDialogReg.dismiss();

//                        Log.d("response", res ponse);

                        if (response != null) {

                            boolean status = JsonParser.checkStatus(response);

                           if(status){

                               SharedPreferences sharedPreferences =
                                       getSharedPreferences(Config.PREFERENCE_KEY, Context.MODE_PRIVATE);

                               SharedPreferences.Editor editor = sharedPreferences.edit();
                               editor.putString(Config.ACCESS_TOKEN, JsonParser.getTokenFromRegistration(response));
                               editor.putString(Config.USER_EMAIL, email);
                               editor.putString(Config.USER_NAME, username);
                               editor.putString(Config.IMAGE_URL, "");
                               editor.putString(Config.FIRSTNAME, "");
                               editor.putString(Config.LASTNAME, "");
                               editor.putString(Config.COUNTRY, "");
                               editor.putString(Config.WHATSAP_NUMBER, "");
                               editor.apply();


                               SharedPreferences preferences = getSharedPreferences(Config.PREFERENCE_KEY, 0);

                               boolean confirmPayment = preferences.getBoolean(Config.CONFIRM_PAYMENT, false);

                               if(confirmPayment){
                                   Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                   startActivity(intent);
                                   finish();

                               }else{
                                   verifyPaymentStatus();

                               }
                           }
                           else{
                               Toast.makeText(getApplication(), JsonParser.parseFeedbackMessage(response), Toast.LENGTH_SHORT).show();
                           }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialogReg.dismiss();

                Toast.makeText(getApplication(), "Oops:) something went wrong", Toast.LENGTH_LONG).show();
            }
        }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(Config.USER_EMAIL, email);
                params.put(Config.USER_NAME, username);
                params.put(Config.IMAGE_URL, "");
                params.put(Config.PASSWORD, password);
                params.put(Config.FIRSTNAME, "");
                params.put(Config.LASTNAME, "");
                params.put(Config.COUNTRY, "");
                params.put(Config.WHATSAP_NUMBER, "");

                return params;
            }
        };

        queue.add(postRequest);
    }

    private void onClickRegButton(){

        username = usernameView.getText().toString();
        email = emailView.getText().toString();
        password = passwordView.getText().toString();

        if (TextUtils.isEmpty(username)) {

            usernameView.setError(getResources().getString(R.string.username_error));

            focusView = usernameView;
            focusView.requestFocus();

            cancel = true;
        }else if (TextUtils.isEmpty(email)) {

            emailView.setError(getResources().getString(R.string.email_error));

            focusView = emailView;
            focusView.requestFocus();

            cancel = true;
        }else if (!Utility.isValidEmail(email)) {

            emailView.setError(getResources().getString(R.string.email_error));

            focusView = emailView;
            focusView.requestFocus();

            cancel = true;
        }
        else if (TextUtils.isEmpty(password)) {

            passwordView.setError(getResources().getString(R.string.password_error));

            focusView = passwordView;
            focusView.requestFocus();

            cancel = true;
        }else if (password.length() < 7) {

            passwordView.setError(getResources().getString(R.string.password_length_error));

            focusView = passwordView;
            focusView.requestFocus();

            cancel = true;
        }else
            registration();

    }

    private void onClickLoginButton(){

        email = emailView.getText().toString();
        password = passwordView.getText().toString();


        if (TextUtils.isEmpty(email)) {

            emailView.setError(getResources().getString(R.string.email_error));

            focusView = emailView;
            focusView.requestFocus();

            cancel = true;
        }else if (TextUtils.isEmpty(password)) {

            passwordView.setError(getResources().getString(R.string.password_error));

            focusView = passwordView;
            focusView.requestFocus();

            cancel = true;
        }
        else
            signIn();


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.loginButton:
                onClickLoginButton();
                break;

            case R.id.getStartedButton:
                onClickRegButton();
                break;

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_manual_registration_and_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == android.R.id.home){

            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void verifyPaymentStatus(){

        paymentStatusDialog.show();

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        StringRequest postRequest = new StringRequest(Request.Method.POST, UrlConstant.PAYSTACK_VERIFY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response != null) {

                            paymentStatusDialog.dismiss();

                            boolean confirmPayment = JsonParser.checkStatus(response);

                            if (confirmPayment) {

                                SharedPreferences sharedPreferences =
                                        getSharedPreferences(Config.PREFERENCE_KEY, Context.MODE_PRIVATE);

                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putBoolean(Config.CONFIRM_PAYMENT, true);
                                editor.apply();

                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                                finish();

                            } else {

                                Intent intent = new Intent(ManualRegistrationAndLogin.this, PayWithPayStackActivity.class);
                                startActivity(intent);
                                finish();
                            }


                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                paymentStatusDialog.dismiss();

                Toast.makeText(getApplication(), "Oops:) something went wrong, please try again", Toast.LENGTH_LONG).show();
            }
        }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                SharedPreferences preferences = getSharedPreferences(Config.PREFERENCE_KEY, 0);

                String accessToken = preferences.getString(Config.ACCESS_TOKEN, "");

                Map<String, String> params = new HashMap<>();

                params.put(Config.ACCESS_TOKEN, accessToken);
                return params;
            }
        };

        queue.add(postRequest);
    }
}
