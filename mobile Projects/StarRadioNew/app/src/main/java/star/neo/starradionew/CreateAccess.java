package star.neo.starradionew;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.gc.materialdesign.views.ButtonRectangle;
import com.vstechlab.easyfonts.EasyFonts;

public class CreateAccess extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       setContentView(R.layout.activity_create_access);


        findViewById(R.id.forgot_passsword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateAccess.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        ButtonRectangle createAccountButton = (ButtonRectangle) findViewById(R.id.createAccount);
        createAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(CreateAccess.this, ManualRegistrationAndLogin.class);
                startActivity(intent);
                finish();
            }
        });
    }

}
