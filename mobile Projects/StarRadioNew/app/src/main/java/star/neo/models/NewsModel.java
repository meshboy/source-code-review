package star.neo.models;

/**
 * Created by mesh on 6/24/16.
 */
public class NewsModel {

    private String newsTitle;
    private String newSubContent;
    private String categoryName;
    private String newsImageUrl;
    private String newsTimeCreated;
    private String fullNewsContent;
    private String newsCreatorImage;
    private String newsAuthor;
    private String newsLink;

    public String getNewsLink() {
        return newsLink;
    }

    public void setNewsLink(String newsLink) {
        this.newsLink = newsLink;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
    public String getNewsAuthor() {
        return newsAuthor;
    }

    public void setNewsAuthor(String newsAuthor) {
        this.newsAuthor = newsAuthor;
    }


    public String getNewsImageUrl() {
        return newsImageUrl;
    }

    public void setNewsImageUrl(String newsImageUrl) {
        this.newsImageUrl = newsImageUrl;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewSubContent() {
        return newSubContent;
    }

    public void setNewSubContent(String newSubContent) {
        this.newSubContent = newSubContent;
    }

    public String getNewsTimeCreated() {
        return newsTimeCreated;
    }

    public void setNewsTimeCreated(String newsTimeCreated) {
        this.newsTimeCreated = newsTimeCreated;
    }

    public String getFullNewsContent() {
        return fullNewsContent;
    }

    public void setFullNewsContent(String fullNewsContent) {
        this.fullNewsContent = fullNewsContent;
    }

    public String getNewsCreatorImage() {
        return newsCreatorImage;
    }

    public void setNewsCreatorImage(String newsCreatorImage) {
        this.newsCreatorImage = newsCreatorImage;
    }

}
