package star.neo.models;

/**
 * Created by mesh on 6/24/16.
 */
public class CommentsModel {

    private String commentCreatorName;
    private String comment;
    private String commentTime;
    private String commentCreatorImageUrl;

    public String getCommentCreatorName() {
        return commentCreatorName;
    }

    public void setCommentCreatorName(String commentCreatorName) {
        this.commentCreatorName = commentCreatorName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(String commentTime) {
        this.commentTime = commentTime;
    }

    public String getCommentCreatorImageUrl() {
        return commentCreatorImageUrl;
    }

    public void setCommentCreatorImageUrl(String commentCreatorImageUrl) {
        this.commentCreatorImageUrl = commentCreatorImageUrl;
    }


}
