package star.neo.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import star.neo.starradionew.R;

/**
 * Created by mesh on 7/6/16.
 */
public class TeamViewHolder extends RecyclerView.ViewHolder {

    ImageView presenterImage;
    TextView presentersName;

    public TeamViewHolder(View itemView) {
        super(itemView);

        presenterImage = (ImageView) itemView.findViewById(R.id.team_image);
        presentersName = (TextView) itemView.findViewById(R.id.presenter_name);
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }
}
