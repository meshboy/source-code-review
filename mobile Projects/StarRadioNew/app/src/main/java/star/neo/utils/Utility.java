package star.neo.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mesh on 6/25/16.
 */
public class Utility {
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    public static String extractImgFromHtml(String html){

        String IMG_REGEX = "<[iI][mM][gG][^>]+[sS][rR][cC]\\\\s*=\\\\s*['\\\"]([^'\\\"]+)['\\\"][^>]*>";

        Pattern pattern = Pattern.compile(IMG_REGEX);
        Matcher matcher = pattern.matcher(html);

        if(matcher.find())
            return matcher.group(1);

        return  null;
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static JSONObject mergeJsonObjects (JSONObject jsonObjectOne, JSONObject jsonObjectTwo) throws JSONException {

        JSONObject mergedJSON = new JSONObject();

        Iterator iteratorOne = jsonObjectOne.keys();
        Iterator iteratorTwo = jsonObjectTwo.keys();

        String tmpKey;

        while (iteratorOne.hasNext()){

            tmpKey = (String)iteratorOne.next();

            mergedJSON.put(tmpKey, jsonObjectOne.get(tmpKey));
        }

        while(iteratorTwo.hasNext()){

            tmpKey = (String) iteratorTwo.next();

            mergedJSON.put(tmpKey, jsonObjectTwo.get(tmpKey));
        }

        return mergedJSON;
    }

}
