package star.neo.adapters;

/**
 * Created by mesh on 6/5/16.
 */
public interface OnLoadMoreListener {

    void loadMore();
}
