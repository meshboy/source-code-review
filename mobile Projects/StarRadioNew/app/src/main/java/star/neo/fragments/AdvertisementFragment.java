package star.neo.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.gc.materialdesign.views.ButtonRectangle;

import star.neo.starradionew.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdvertisementFragment extends Fragment {

    ButtonRectangle proceedButton;

    boolean isConditionAccepted = false;

    public AdvertisementFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_advertisement, container, false);



        proceedButton = (ButtonRectangle) view.findViewById(R.id.proceedButton);
        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activateAdvertPage();
            }
        });


        return view;
    }

    private void activateAdvertPage(){

        FragmentManager fragmentManager  = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.containerView, new PlaceAdvertFragment()).addToBackStack(null).commit();

//        new MaterialDialog.Builder(getActivity())
//                .title("Terms And Condition")
//                .items(R.array.terms_and_condition)
//                .itemsCallback(new MaterialDialog.ListCallback() {
//                    @Override
//                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
//                    }
//                })
//                .positiveText("Accept Condition")
//                .negativeText("close")
//                .negativeColor(ContextCompat.getColor(getActivity(), R.color.md_red_400))
//                .positiveColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark))
//                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
//                        FragmentManager fragmentManager  = getActivity().getSupportFragmentManager();
//                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                        fragmentTransaction.replace(R.id.containerView, new PlaceAdvertFragment()).addToBackStack(null).commit();
//                    }
//                })
//                .onNegative(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
//                        materialDialog.dismiss();
//                    }
//                }).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        for (Fragment fragment : getChildFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

}
