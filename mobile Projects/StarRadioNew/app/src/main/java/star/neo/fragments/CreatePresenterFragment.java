package star.neo.fragments;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gc.materialdesign.views.ButtonRectangle;

import java.util.HashMap;
import java.util.Map;

import star.neo.starradionew.R;
import star.neo.utils.Config;
import star.neo.utils.JsonParser;
import star.neo.utils.UrlConstant;


public class CreatePresenterFragment extends Fragment {

    EditText roleModelView, bioTextView;

    boolean cancel = false;

    View focusView = null;

    CoordinatorLayout coordinatorLayout;

    ProgressDialog progressDialog;

    String accessToken;

    String roleModelRequest, bioRequest;

    TextView checkOUtTeamMembersView;

    ButtonRectangle submitPresentation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_presenter, container, false);


        roleModelView = (EditText) view.findViewById(R.id.role_model);
        bioTextView = (EditText) view.findViewById(R.id.bio);

        SharedPreferences preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);

        accessToken = preferences.getString(Config.ACCESS_TOKEN, "");

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.progress_present));

        coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinatorLayoutPresent);

        submitPresentation = (ButtonRectangle) view.findViewById(R.id.submitPresentation);

        view.findViewById(R.id.submitPresentation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendPresentationRequest();
            }
        });


        checkOUtTeamMembersView = (TextView) view.findViewById(R.id.instruction_click);
        checkOUtTeamMembersView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                meetTeam();
            }
        });

        return view;
    }

    public void sendPresentationRequest() {

        roleModelView.setError(null);
        bioTextView.setError(null);

        roleModelRequest = roleModelView.getText().toString();
        bioRequest = bioTextView.getText().toString();

        if (TextUtils.isEmpty(bioRequest)) {

            bioTextView.setError(getResources().getString(R.string.desc_error));

            focusView = bioTextView;
            focusView.requestFocus();

        } else if (TextUtils.isEmpty(roleModelRequest)) {

            roleModelView.setError(getResources().getString(R.string.create_error));

            focusView = roleModelView;
            focusView.requestFocus();
        } else {

            progressDialog.show();

            RequestQueue queue = Volley.newRequestQueue(getActivity());

            StringRequest postRequest = new StringRequest(Request.Method.POST, UrlConstant.PRESENTER_ADD_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            if (response != null) {

                                progressDialog.dismiss();

                                roleModelView.setText("");
                                bioTextView.setText("");

                                new MaterialDialog.Builder(getActivity())
                                        .title("Team's Feedback")
                                        .titleColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark))
                                        .content(JsonParser.parseFeedbackMessage(response))
                                        .positiveText("meet our Team")
                                        .negativeText("close")
                                        .negativeColor(ContextCompat.getColor(getActivity(), R.color.md_red_400))
                                        .positiveColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark))
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                                                meetTeam();
                                            }
                                        })
                                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                                                materialDialog.dismiss();
                                            }
                                        })
                                        .show();


                                Toast.makeText(getActivity(), JsonParser.parseFeedbackMessage(response), Toast.LENGTH_LONG).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.dismiss();

                    submitPresentation.setVisibility(View.GONE);

                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "check your network connection", Snackbar.LENGTH_LONG);

                    // Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            submitPresentation.setVisibility(View.VISIBLE);
                        }
                    }, 3000);
                }
            }

            ) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();
                    params.put(Config.PRESENTER_ROLE_MODEL, roleModelRequest);
                    params.put(Config.PRESENTER_BIO, bioRequest);
                    params.put(Config.ACCESS_TOKEN, accessToken);

                    return params;
                }
            };

            queue.add(postRequest);
        }
    }

    public void meetTeam() {

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.containerView, new StarRadioTeamFragment()).addToBackStack(null).commit();
    }
}

