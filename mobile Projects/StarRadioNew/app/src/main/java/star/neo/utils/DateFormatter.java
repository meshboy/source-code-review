package star.neo.utils;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by root on 6/14/15.
 */
public class DateFormatter {

    private static String parseSimpleFormatter (Date dateTime){

        Date now = new Date();

        long checkTime = (now.getTime() - dateTime.getTime())/1000;

        long sec = (checkTime >= 60 ? checkTime % 60 : checkTime);
        long min = (checkTime = (checkTime / 60)) >= 60 ? checkTime % 60 : checkTime;
        long hrs = (checkTime = (checkTime / 60)) >= 24 ? checkTime % 24 : checkTime;
        long days = (checkTime = (checkTime / 24)) >= 30 ? checkTime % 30 : checkTime;
        long months = (checkTime = (checkTime / 30)) >= 12 ? checkTime % 12 : checkTime;
        long years = (checkTime = (checkTime / 12));

        StringBuffer sb = new StringBuffer();

        if (years > 0) {
            if (years == 1) {
                sb.append("a year");
            } else {
                sb.append(years + " years");
            }
//                if (years <= 6 && months > 0) {
//                    if (months == 1) {
//                        sb.append(" and a month");
//                    } else {
//                        sb.append(" and " + months + " months");
//                    }
//                }
        } else if (months > 0) {
            if (months == 1) {
                sb.append("a month");
            } else {
                sb.append(months + " months");
            }
//                if (months <= 6 && days > 0) {
//                    if (days == 1) {
//                        sb.append(" and a day");
//                    } else {
//                        sb.append(" and " + days + " days");
//                    }
//                }
        } else if (days > 0) {
            if (days == 1) {
                sb.append("a day");
            } else {
                sb.append(days + " days");
            }
//                if (days <= 3 && hrs > 0) {
//                    if (hrs == 1) {
//                        sb.append(" and an hour");
//                    } else {
//                        sb.append(" and " + hrs + " hours");
//                    }
//                }
        } else if (hrs > 0) {
            if (hrs == 1) {
                sb.append("an hour");
            } else {
                sb.append(hrs + " hours");
            }
//                if (min > 1) {
//                    sb.append(" and " + min + " minutes");
//                }
        } else if (min > 0) {
            if (min == 1) {
                sb.append("a minute");
            } else {
                sb.append(min + " minutes");
            }
//                if (sec > 1) {
//                    sb.append(" and " + sec + " seconds");
//                }
        } else {
            if (sec <= 1) {
                sb.append("about a second");
            } else {
                sb.append("about " + sec + " seconds");
            }
        }

        sb.append(" ago");

        return sb.toString();


    }

    public static String parseDate (String date){

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            Date realDate = simpleDateFormat.parse(date);

            return String.valueOf(parseSimpleFormatter(realDate));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getTimeStamp(String dateStr) {
        try{
            StringBuilder buffer = new StringBuilder();

            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateStr);

            Calendar calendar = new GregorianCalendar(date.getYear() + 1900, date.getMonth(), date.getDate(), date.getHours(), date.getMinutes());

            String hour = String.valueOf(calendar.get(Calendar.HOUR));
            String minute = String.valueOf(calendar.get(Calendar.MINUTE));
            String hourOfDay = calendar.get(Calendar.HOUR_OF_DAY) >=12 ? "P.M" : "A.M";

            date = calendar.getTime();


            String [] array = date.toString().split(" ");



            for (int i = 0; i < array.length; i++) {

                String space = " ";
                if(i == 4){
                    continue;
                }
                else if(i == 2){
                    space = "";
                }
                else if(i == 3){

                    array[i] = ",";

                }
                buffer.append(array[i]).append(space);
            }

            buffer.append("*").append(hour).append(" : ").append(minute).append(" ").append(hourOfDay);

            return buffer.toString();
        }
        catch(ParseException ex){
            return null;
        }
    }

}
