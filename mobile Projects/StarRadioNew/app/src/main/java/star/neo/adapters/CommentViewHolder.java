package star.neo.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import star.neo.starradionew.R;

/**
 * Created by mesh on 6/26/16.
 */
public class CommentViewHolder extends RecyclerView.ViewHolder {

    TextView nameTextView, commentTextView, timeCreated;

    CircleImageView profileImage;

    public CommentViewHolder(View itemView) {
        super(itemView);

        nameTextView = (TextView) itemView.findViewById(R.id.name);
        commentTextView = (TextView) itemView.findViewById(R.id.comment);
        timeCreated = (TextView) itemView.findViewById(R.id.time);

        profileImage = (CircleImageView) itemView.findViewById(R.id.image_profile);
    }
}
