package star.neo.models;

/**
 * Created by mesh on 7/5/16.
 */
public class SchedulesModel {

    private String id;
    private String schedule;
    private String scheduleTimeFrom;
    private String scheduleTimeTo;
    private String scheduleImageClipArtUrl;
    private String scheduleFeaturedName;
    private boolean scheduleStatus;


    public String getScheduleFeaturedName() {
        return scheduleFeaturedName;
    }

    public void setScheduleFeaturedName(String scheduleFeaturedName) {
        this.scheduleFeaturedName = scheduleFeaturedName;
    }

    public boolean isScheduleStatus() {
        return scheduleStatus;
    }

    public void setScheduleStatus(boolean scheduleStatus) {
        this.scheduleStatus = scheduleStatus;
    }

    public String getScheduleImageClipArtUrl() {
        return scheduleImageClipArtUrl;
    }

    public void setScheduleImageClipArtUrl(String scheduleImageClipArtUrl) {
        this.scheduleImageClipArtUrl = scheduleImageClipArtUrl;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getScheduleTimeFrom() {
        return scheduleTimeFrom;
    }

    public void setScheduleTimeFrom(String scheduleTimeFrom) {
        this.scheduleTimeFrom = scheduleTimeFrom;
    }

    public String getScheduleTimeTo() {
        return scheduleTimeTo;
    }

    public void setScheduleTimeTo(String scheduleTimeTo) {
        this.scheduleTimeTo = scheduleTimeTo;
    }

}
