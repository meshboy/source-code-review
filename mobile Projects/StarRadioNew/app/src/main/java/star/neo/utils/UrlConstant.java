package star.neo.utils;

/**
 * Created by mesh on 6/22/16.
 */
public class UrlConstant {

    public static String BASE_URL = "http://starradio.com.ng/starRadioApi/web/api/v1/";

    public static String API_KEY = "?key=f075e50f46a1dfae98127432b078908c";

    //    USERS                     
    public static String LOGIN_URL = BASE_URL + "user/login" + API_KEY;
    public static String REGISTRATION_URL = BASE_URL + "user/create" + API_KEY;
    public static String GET_SINGLE_USER_URL = BASE_URL + "user/me" + API_KEY;
    public static String UPDATE_USER_URL = BASE_URL + "profile/update" + API_KEY;
    public static String CHANGE_PASSWORD = BASE_URL + "user/change-password" + API_KEY;
    public static String UPDATE_IMAGE_URL = BASE_URL + "profile/image-upload" + API_KEY;

    //    COMMENTS
    public static String GET_ALL_COMMENTS = BASE_URL + "comment" + API_KEY;
    public static String ADD_COMMENTS = BASE_URL + "comment/add" + API_KEY;


    //    REQUESTS
    public static String ADD_REQUEST = BASE_URL + "request/add" + API_KEY;
    public static String GET_REQUEST = BASE_URL + "request" + API_KEY;


    //    LOCATIONS
    public static String ADD_LOCATIONS = BASE_URL + "location/add" + API_KEY;

    //  NEONATAR FEED
    public static String NEONATAR_RSS_URL = "http://neonatar.com.ng/feed/";

    public static String NEONATAR_JSON_URL = "https://ajax.googleapis.com/ajax/services/feed/load?v=2.0&q=" + NEONATAR_RSS_URL;

    //    SPREAKER
    public static String SPREAKER_URL = "https://www.spreaker.com/user/" + Config.SPREAKER_USER_TOKEN;

    public static String EPISODES_URL = "https://api.spreaker.com/user/" + Config.SPREAKER_USER_TOKEN + "/episodes";

    public static String SINGLE_EPISODE = "https://api.spreaker.com/episode/";

    public static String LIVE_RADIO_URL = "http://api.spreaker.com/listen/user/" + Config.SPREAKER_USER_TOKEN + "/episode/latest/shoutcast.mp3";

    //    SOCIALS
    public static String FACEBOOK_SOCIAL = "https://www.facebook.com/STARRADIOOFFICIAL/";
    public static String INSTAGRAM_SOCIAL = "https://www.instagram.com/starradioofficial";
    public static String TWITTER_SOCIAL = "https://twitter.com/star965radio";

    //    SCHEDULES
    public static String SHEDULE_URL = BASE_URL + "schedule" + API_KEY;

    //    PRESENTER
    public static String PRESENTER_ADD_URL = BASE_URL + "presenter/submit" + API_KEY;
    public static String APPROVED_PRESENTERS_URL = BASE_URL + "presenter/approved" + API_KEY;

//    ADVERTS
    public static String ADVERTISEMENT_URL = BASE_URL + "advert/place-advert" + API_KEY;

//    WEBSITE
    public static String WEBSITE_URL = "http://starradio.com.ng";

    public static String FORGOT_PASSWORD = BASE_URL + "user/forgot-password" + API_KEY;

//    PAYMENT URL
    public static String ADD_PAYMENT_URL = BASE_URL + "user/add-payment" + API_KEY;

    public static String PAYSTACK_VERIFY = BASE_URL + "user/verify-payment" + API_KEY;

//    LOG OUT URL
    public static String LOG_OUT_URL = BASE_URL + "user/logout" + API_KEY;

//    SIGN COUNT URL
    public static String LOGIN_COUNT_URL = BASE_URL + "user/login-count" + API_KEY;
}
