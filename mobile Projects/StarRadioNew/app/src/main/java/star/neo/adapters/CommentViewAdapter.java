package star.neo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.volley.toolbox.ImageLoader;
import java.util.Collections;
import java.util.List;
import star.neo.models.CommentsModel;
import star.neo.starradionew.R;
import star.neo.utils.DateFormatter;
import star.neo.utils.MySingleton;


/**
 * Created by mesh on 6/26/16.
 */
public class CommentViewAdapter extends RecyclerView.Adapter<CommentViewHolder> {

    List<CommentsModel>  commentsModelList = Collections.emptyList();
    Context context;

    ImageLoader mImageLoader;
    OnLoadMoreListener onLoadMoreListener;

    public CommentViewAdapter(List<CommentsModel> commentsModelList, Context context) {

        this.commentsModelList = commentsModelList;
        this.context = context;
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_comment_row, parent, false);

        CommentViewHolder viewHolder = new CommentViewHolder(view);

        return  viewHolder;

    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {

        mImageLoader = MySingleton.getInstance(context.getApplicationContext())
                .getImageLoader();

        mImageLoader.get(commentsModelList.get(position).getCommentCreatorImageUrl(),
                ImageLoader.getImageListener(holder.profileImage, R.drawable.userimage, R.drawable.userimage));

        holder.commentTextView.setText(commentsModelList.get(position).getComment());
        holder.nameTextView.setText(commentsModelList.get(position).getCommentCreatorName());
        holder.timeCreated.setText(DateFormatter.getTimeStamp(commentsModelList.get(position).getCommentTime()));
    }

    @Override
    public int getItemCount() {

        try{
            return commentsModelList.size();
        }
        catch(Exception ex){
            return 0;
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
