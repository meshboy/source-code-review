package star.neo.fragments;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import java.util.ArrayList;
import java.util.List;

import star.neo.adapters.NewsViewHolder;
import star.neo.adapters.RecyclerNewsViewAdapter;
import star.neo.models.NewsModel;
import star.neo.starradionew.NewsDetailsActivity;
import star.neo.starradionew.R;
import star.neo.utils.Config;
import star.neo.utils.JsonParser;
import star.neo.utils.UrlConstant;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment  {

    public String LOG_TAG = NewsFragment.this.getClass().getSimpleName();

    RecyclerView recyclerView;

    RecyclerNewsViewAdapter adapter;

    List<NewsModel> newsModelList;

    SwipeRefreshLayout swipeRefreshLayout;

    ProgressBar progressBar;

    String category;

    LinearLayoutManager linearLayoutManager = null;

    boolean isRefreshing = true;



    Handler handler = new Handler();

    public static NewsFragment newsInstance(Bundle os) {
        // Required empty public constructor

        NewsFragment newsFragment = new NewsFragment();
        newsFragment.setArguments(os);

        return newsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news, container, false);


        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);

        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_dark,
                android.R.color.holo_red_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // get the new data from you data source
                // TODO : request data here
                // our swipeRefreshLayout needs to be notified when the data is returned in order for it to stop the animation
                showProgressBar(false);
                loadNews();
                handler.post(refreshing);

            }
        });


//        get category name and set on toolbar
        Bundle bundle = getArguments();
        category = bundle.getString(Config.CATEGORY_BUNDLE);

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(category);

        linearLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                // TODO Auto-generated method stub
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                // TODO Auto-generated method stub
                //super.onScrollStateChanged(recyclerView, newState);
                int firstPos = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                if (firstPos > 0) {
                    swipeRefreshLayout.setEnabled(false);
                } else {
                    swipeRefreshLayout.setEnabled(true);
                }
            }
        });

        loadNews();


        return view;
    }

    private final Runnable refreshing = new Runnable(){
        public void run(){
            try {
                // TODO : isRefreshing should be attached to your data request status
                if(isRefreshing){
                    // re run the verification after 1 second
                    handler.postDelayed(this, 1000);
                }else{
                    // stop the animation after the data is fully loaded
                    swipeRefreshLayout.setRefreshing(false);
                    // TODO : update your list with the new data
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void loadNews() {

        showProgressBar(true);

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest strReq = new StringRequest(Request.Method.GET,
                UrlConstant.NEONATAR_JSON_URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                if (response != null) {

                    isRefreshing = false;

                    showProgressBar(false);

//                   cache fresh news
                    try {

                        SharedPreferences preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);
                        String jsonResponse = preferences.getString(Config.JSON_RESPONSE, "");
//
//                        if(jsonResponse)

                        processJsonResponse(response);

                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(Config.JSON_RESPONSE, response);
                        editor.apply();
                    } catch (Exception ex) {

                        Log.d(LOG_TAG, "error" + ex.getMessage());
                    }



                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                showProgressBar(false);

                isRefreshing = false;

                Toast.makeText(getActivity(), "Oops:) something went wrong. please try again...", Toast.LENGTH_LONG).show();
                try{

                    SharedPreferences preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);
                    String jsonResponse = preferences.getString(Config.JSON_RESPONSE, "");

                    processJsonResponse(jsonResponse);
                }
                catch (Exception ex){

                }
            }
        });

        //Adding request to request queue
        queue.add(strReq);
    }

    public void processJsonResponse(String response) {

        Log.d(LOG_TAG, response);

        newsModelList = JsonParser.parseGetNews(response);

        if(newsModelList.size() > 0){

            if(!(getNewsByCategory(newsModelList, category).size() > 0))
                Toast.makeText(getActivity(), "No item found", Toast.LENGTH_LONG).show();

            recyclerView.setLayoutManager(determineScreenSize());
            adapter = new RecyclerNewsViewAdapter(getNewsByCategory(newsModelList, category), getActivity());
            recyclerView.setAdapter(adapter);

            recyclerView.addOnItemTouchListener(new RecyclerNewsViewAdapter.RecyclerTouchListener(getActivity(), recyclerView, new NewsViewHolder.ClickListener() {

                @Override
                public void onClick(View view, int position) {
//                    TODO: respond to an item click



                    Intent intent = new Intent(getActivity(), NewsDetailsActivity.class);
                    intent.putExtra("title", (getNewsByCategory(newsModelList, category).get(position).getNewsTitle()));
                    intent.putExtra("content", (getNewsByCategory(newsModelList, category).get(position).getFullNewsContent()));
                    intent.putExtra("time_created", (getNewsByCategory(newsModelList, category).get(position).getNewsTimeCreated()));
                    intent.putExtra("author", (getNewsByCategory(newsModelList, category).get(position).getNewsAuthor()));
                    intent.putExtra("image_url", (getNewsByCategory(newsModelList, category).get(position).getNewsImageUrl()));
                    intent.putExtra("category", "Star Radio | " + category);
                    intent.putExtra("link", (getNewsByCategory(newsModelList, category).get(position).getNewsLink()));
                    startActivity(intent);
                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }));
        }
        else
            Toast.makeText(getActivity(), "News list empty", Toast.LENGTH_LONG).show();

    }


    public static List<NewsModel> getNewsByCategory(List<NewsModel> modelList, String categoryString) {

        List<NewsModel> categoryList = new ArrayList<>();

        //        get news list by its category name
        if (modelList != null && !TextUtils.isEmpty(categoryString)) {

            for (int i = 0; i < modelList.size(); i++) {

                if (modelList.get(i).getCategoryName().equals(categoryString)) {

                    NewsModel categoryNewsModel = new NewsModel();
                    categoryNewsModel.setCategoryName(modelList.get(i).getCategoryName());
                    categoryNewsModel.setNewsTitle(modelList.get(i).getNewsTitle());
                    categoryNewsModel.setNewsAuthor(modelList.get(i).getNewsAuthor());
                    categoryNewsModel.setNewSubContent(modelList.get(i).getNewSubContent());
                    categoryNewsModel.setFullNewsContent(modelList.get(i).getFullNewsContent());
                    categoryNewsModel.setNewsImageUrl(modelList.get(i).getNewsImageUrl());
                    categoryNewsModel.setNewsTimeCreated(modelList.get(i).getNewsTimeCreated());

                    categoryList.add(categoryNewsModel);
                }
            }
        }


        return categoryList;
    }

    public void showProgressBar(boolean isShowing) {

        if (isShowing) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    /**
     *
     * @return the corresponding layout for screen size
     */

    private RecyclerView.LayoutManager determineScreenSize (){

        switch ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)){

            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                return new GridLayoutManager(getActivity(), 2);

            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                return new LinearLayoutManager(getActivity());

            case Configuration.SCREENLAYOUT_SIZE_MASK:
                return new LinearLayoutManager(getActivity());

            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                return new LinearLayoutManager(getActivity());

            default:
                return new LinearLayoutManager(getActivity());
        }

    }
}
