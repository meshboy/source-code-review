package star.neo.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gc.materialdesign.views.ButtonRectangle;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import star.neo.models.ImageModel;
import star.neo.starradionew.ImageActivity;
import star.neo.starradionew.R;
import star.neo.utils.Config;
import star.neo.utils.JsonParser;
import star.neo.utils.MySingleton;
import star.neo.utils.UrlConstant;


public class SettingsFragment extends Fragment {

    String imageUrl;

    EditText firstNameView, lastNameView, usernameView, newPasswordText, currentPasswordText, whatsappNumberText;

    ButtonRectangle changePasswordButton;

    TextView emailView;

    String username, email, firstname, lastname, accessToken, userChosenTask, whatsappNumber;

    View focusView = null;

    ProgressDialog progressDialogUpdate;

    ImageLoader mImageLoader;

    CircleImageView userImageProfile;

    int PICK_IMAGE_REQUEST = 1;

    Bitmap imageBitmap = null;

    ProgressBar progressBar;


    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        emailView = (TextView) view.findViewById(R.id.email);

        firstNameView = (EditText) view.findViewById(R.id.firstname);
        lastNameView = (EditText) view.findViewById(R.id.lastname);
        usernameView = (EditText) view.findViewById(R.id.username);
        whatsappNumberText = (EditText) view.findViewById(R.id.whatsap_number);

        progressBar = (ProgressBar) view.findViewById(R.id.progress);

        view.findViewById(R.id.my_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), ImageActivity.class);
                startActivity(intent);
            }
        });

        view.findViewById(R.id.add_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                chooseImageFile();
            }
        });

        view.findViewById(R.id.updateButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUser();
            }
        });

        SharedPreferences preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);

        imageUrl = preferences.getString(Config.IMAGE_URL, "");
        userImageProfile = (CircleImageView) view.findViewById(R.id.my_profile);
        loadImageProfile();

        username = preferences.getString(Config.USER_NAME, "");
        accessToken = preferences.getString(Config.ACCESS_TOKEN, "");
        email = preferences.getString(Config.USER_EMAIL, "");
        firstname = preferences.getString(Config.FIRSTNAME, "");
        lastname = preferences.getString(Config.LASTNAME, "");
        whatsappNumber = preferences.getString(Config.WHATSAP_NUMBER, "");

        firstNameView.setText(firstname);
        lastNameView.setText(lastname);
        emailView.setText(email);
        whatsappNumberText.setText(whatsappNumber);
        usernameView.setText(username);

        SpannableString spannableStringReg = new SpannableString(getResources().getString(R.string.loading_updating));
        spannableStringReg.setSpan(new ForegroundColorSpan(Color.BLACK), 0, spannableStringReg.length(), 0);
        progressDialogUpdate = new ProgressDialog(getActivity());
        progressDialogUpdate.setMessage(spannableStringReg);

        return view;
    }


    private void loadImageProfile() {

        if (imageUrl != null) {

            // Get the ImageLoader through your singleton class.
//            Glide.with(this)
//                    .load(imageUrl)
//                    .skipMemoryCache(true)
//                    .placeholder(R.drawable.userimage)
//                    .error(R.drawable.userimage)
//                    .into(userImageProfile);

            mImageLoader = MySingleton.getInstance(getActivity())
                    .getImageLoader();

            mImageLoader.get(imageUrl, ImageLoader.getImageListener(userImageProfile,
                    R.drawable.userimage, R.drawable.userimage));
        }
    }

    private void chooseImageFile() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == getActivity().RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();

            uploadFile(getFilePath(filePath));

            try {
                //Getting the Bitmap from Gallery
                imageBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
                userImageProfile.setImageBitmap(imageBitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateUser() {

        usernameView.setError(null);
        firstNameView.setError(null);
        lastNameView.setError(null);

        username = usernameView.getText().toString();
        firstname = firstNameView.getText().toString();
        lastname = lastNameView.getText().toString();
        whatsappNumber = whatsappNumberText.getText().toString();

        if (TextUtils.isEmpty(username)) {

            usernameView.setError(getResources().getString(R.string.username_error));

            focusView = usernameView;
            focusView.requestFocus();

        } else if (TextUtils.isEmpty(firstname)) {

            firstNameView.setError(getResources().getString(R.string.firstname_error));

            focusView = firstNameView;
            focusView.requestFocus();

        } else if (TextUtils.isEmpty(lastname)) {

            lastNameView.setError(getResources().getString(R.string.lastname_error));

            focusView = lastNameView;
            focusView.requestFocus();

        } else if (TextUtils.isEmpty(whatsappNumber)) {

            whatsappNumberText.setError(getResources().getString(R.string.whatsapp_error));

            focusView = whatsappNumberText;
            focusView.requestFocus();

        } else
            processUserUpdate();
    }

    private void processUserUpdate() {

        progressDialogUpdate.show();

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest postRequest = new StringRequest(Request.Method.POST, UrlConstant.UPDATE_USER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response != null) {

                            progressDialogUpdate.dismiss();

                            Toast.makeText(getActivity(), JsonParser.parseFeedbackMessage(response), Toast.LENGTH_LONG).show();

                            if (JsonParser.checkStatus(response)) {
                                SharedPreferences sharedPreferences =
                                        getActivity().getSharedPreferences(Config.PREFERENCE_KEY, Context.MODE_PRIVATE);

                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString(Config.USER_NAME, username);
                                editor.putString(Config.FIRSTNAME, firstname);
                                editor.putString(Config.LASTNAME, lastname);
                                editor.putString(Config.WHATSAP_NUMBER, whatsappNumber);
                                editor.apply();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialogUpdate.dismiss();
                Toast.makeText(getActivity(), "Oops:) something went wrong, please try again...", Toast.LENGTH_LONG).show();
            }
        }

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(Config.USER_NAME, username);
                params.put(Config.ACCESS_TOKEN, accessToken);
                params.put(Config.FIRSTNAME, firstname);
                params.put(Config.LASTNAME, lastname);
                params.put(Config.WHATSAP_NUMBER, whatsappNumber);

                return params;
            }
        };

        queue.add(postRequest);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_setting, menu);  // Use filter.xml from step 1
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.change_password) {
            changePassword();
            return true;
        } else if (id == R.id.log_out) {
            logOUt();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void logOUt() {

    }

    public void uploadFile(String filePath) {

        progressBar.setVisibility(View.VISIBLE);

        RequestParams params = new RequestParams();
        params.put(Config.ACCESS_TOKEN, accessToken);
        try {
            params.put(Config.IMAGE_URL, new File(filePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClient();

        client.post(UrlConstant.UPDATE_IMAGE_URL, params, new BaseJsonHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, String response, Object o) {

                if(response != null){

                    ImageModel imageModel = JsonParser.parseUserImageProfile(response);

                    SharedPreferences preferences = getActivity().getSharedPreferences(Config.PREFERENCE_KEY, 0);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(Config.IMAGE_URL, imageModel.getImageUrl());
                    editor.apply();

                    Log.d("imageurl",  imageModel.getImageUrl());

                    final String message = imageModel.getFeedback();

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            progressBar.setVisibility(View.GONE);

                            Toast.makeText(getActivity(), message , Toast.LENGTH_LONG).show();
                        }
                    });

                }
            }

            @Override
            public void onFailure(int i, Header[] headers, Throwable throwable, String response, Object o) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        progressBar.setVisibility(View.GONE);
                    }
                });

            }

            @Override
            protected Object parseResponse(String s, boolean b) throws Throwable {
                return null;
            }
        });

    }

    private void changePassword() {

        MaterialDialog dialog = new MaterialDialog.Builder(getActivity()).title(getResources().getString(R.string.change_password))
                .customView(R.layout.change_password, true)
                .build();

        View customView = dialog.getView();

        newPasswordText = (EditText) customView.findViewById(R.id.new_password);
        currentPasswordText = (EditText) customView.findViewById(R.id.current_password);

        changePasswordButton = (ButtonRectangle) customView.findViewById(R.id.change_password);

        changePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                newPasswordText.setError(null);
                currentPasswordText.setError(null);

                final String newPassword, currentPassword;

                newPassword = newPasswordText.getText().toString();
                currentPassword = currentPasswordText.getText().toString();

                if (TextUtils.isEmpty(newPassword)) {
                    newPasswordText.setError(getResources().getString(R.string.field_error));
                    focusView = newPasswordText;
                    focusView.requestFocus();

                } else if (TextUtils.isEmpty(currentPassword)) {
                    currentPasswordText.setError(getResources().getString(R.string.field_error));
                    focusView = currentPasswordText;
                    focusView.requestFocus();

                } else if (newPassword.length() < 7) {

                    newPasswordText.setError(getResources().getString(R.string.password_length_error));

                    focusView = newPasswordText;
                    focusView.requestFocus();

                } else {

                    final ProgressDialog progress = new ProgressDialog(getActivity());

                    progress.setMessage(getResources().getString(R.string.loading));
                    progress.show();

                    RequestQueue queue = Volley.newRequestQueue(getActivity());

                    StringRequest postRequest = new StringRequest(Request.Method.POST, UrlConstant.CHANGE_PASSWORD,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                    if (response != null) {

                                        progress.dismiss();

                                        Toast.makeText(getActivity(), JsonParser.parseFeedbackMessage(response), Toast.LENGTH_LONG).show();
                                        if (JsonParser.checkStatus(response)) {
                                            newPasswordText.setText("");
                                            currentPasswordText.setText("");
                                        }

                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            progress.dismiss();
                            Toast.makeText(getActivity(), "Oops:) something went wrong, please try again...", Toast.LENGTH_LONG).show();
                        }
                    }

                    ) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();
                            params.put(Config.PASSWORD, newPassword);
                            params.put(Config.ACCESS_TOKEN, accessToken);
                            params.put(Config.OLD_PASSWORD, currentPassword);

                            return params;
                        }
                    };

                    queue.add(postRequest);
                }
            }
        });


        dialog.show();

    }

    private String getFilePath(Uri uri) {

        String[] projection = {MediaStore.Images.Media.DATA};

        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);

        if (cursor == null) return null;

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

        cursor.moveToFirst();

        String s = cursor.getString(column_index);

        cursor.close();
        return s;
    }
}
